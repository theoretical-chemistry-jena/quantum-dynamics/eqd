"""
Small utilities for the analysis of the db files.
"""


import sqlite3


def sql_json_qry(prop_name: str, position: int, table_name: str = "data") -> str:
    """
    Make a small JSON query for an SQLite database where the value is only a list
    of values.

    Args:
        prop_name (str): [description]
        position (int): [description]
        table_name (str, optional): [description]. Defaults to "data".

    Returns:
        str: [description]
    """
    pattern = f"json_extract({table_name}.{prop_name}, '$[{position}]')"

    return pattern


def test_sql_json_qry(idx: int = 1):
    """Basic test case for the sql_json_qry function."""

    db = sqlite3.connect(":memory:")
    db.execute("CREATE TABLE jsont (vals JSON1)")
    db.executemany(
        "INSERT INTO jsont VALUES (?)",
        [("[0.0, 1.0, 2.0, 3.0]", ), ("[0.0, 2.0, 4.0, 6.0]", )],
    )

    # Read as JSON
    rows = list(
        db.execute(
            f"""SELECT {sql_json_qry("vals", idx, table_name="jsont")} FROM jsont"""
        )
    )
    print(rows)

    row = rows[0]
    print(row)

    assert row[0] == float(idx)
    assert len(row) == 1

    # Destroy the database
    del db


if __name__ == "__main__":
    test_sql_json_qry()
