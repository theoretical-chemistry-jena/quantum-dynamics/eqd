#!/usr/bin/env python
# coding: utf-8

import datetime
import itertools as it
import multiprocessing
from pathlib import Path
from operator import add, sub
from functools import partial

import numba
import numpy as np
import sympy as sym

from sympy.matrices import Matrix
from tqdm import tqdm
from eQD.calculation import Calculation
from eQD.utils import fieldstrength_sympy as fieldstrength


# Define all the necessary sympy symbols.
T, PHI, DT, W, EPSILON, V, J, DT1, DT2, DT3, MU1, MU2, E1, E2, TM, TP = sym.symbols(
    "t phi dt omega epsilon V J dt_1 dt_2 dt_3 mu_1 mu_2 E_1 E_2, tau_m, tau_p"
)

FIELD01_CPHI = E1 * (sym.cos(W * T) + sym.cos(W * T + PHI))
FIELD02_CPHI = E2 * (sym.cos(W * T) + sym.cos(W * T + PHI + EPSILON))
FIELD01 = E1 * 2 * sym.cos(W * T)
FIELD02 = E2 * 2 * sym.cos(W * T)


def integrator(subkw: dict, disable_tqdm=True):

    # pprint(subkw, stream=TqdmStream)

    opts = subkw.pop("opts")

    uphi, correct_intensity = opts["use_phi"], opts["intcorr"]
    adiab, off_resonant = opts["adiab"], opts["off_resonant"]
    ts = opts["tgrid"]
    outpath = opts["outpath"]


    # Hamiltonian
    hamiltonian = Matrix([
        [0, 0, 0],
        [0, V, J],
        [0, J, V]
    ])

    umat, diag = hamiltonian.diagonalize()

    igamma = Matrix([
        [0, 0, 0],
        [0, -1j * (1.0 / TM), 0],
        [0, 0, -1j * (1.0 / TP)]
    ])

    if adiab:
        igamma_transf = umat * igamma * umat**-1
    else:
        igamma_transf = igamma

    exp_prop_dt4 = sym.exp(
        -1j * DT3 * (hamiltonian + igamma_transf)
    )

    # Field specification
    if not uphi and not correct_intensity:
        field1_phi = sym.lambdify(T, FIELD01_CPHI.subs(subkw))
        field2_phi = sym.lambdify(T, FIELD02_CPHI.subs(subkw))

        field1_orig = sym.lambdify(T, FIELD01.subs(subkw))
        field2_orig = sym.lambdify(T, FIELD02.subs(subkw))

        e1 = fieldstrength(field1_phi, ts)
        e2 = fieldstrength(field2_phi, ts)

        e1m = fieldstrength(field1_orig, ts)
        e2m = fieldstrength(field2_orig, ts)

        subkw.update(E_1=subkw["E_1"] * e1 / e1m, E_2=subkw["E_2"] * e2 / e2m)

        # Re-testing
        field1_test = sym.lambdify(T, FIELD01.subs(subkw))
        field2_test = sym.lambdify(T, FIELD02.subs(subkw))
        e1t = fieldstrength(field1_test, ts)
        e2t = fieldstrength(field2_test, ts)

        tqdm.write(
            f"PMimick // E1: {e1:.3e}, E1t: {e1t:.3e}\nE2: {e2:.3e}, E2t: {e2t:.3e}\n"
        )

        symfields = FIELD01, FIELD02

    elif uphi and correct_intensity:
        field1_phi = sym.lambdify(T, FIELD01_CPHI.subs(subkw))
        field2_phi = sym.lambdify(T, FIELD02_CPHI.subs(subkw))

        e1 = fieldstrength(field1_phi, ts)
        e2 = fieldstrength(field2_phi, ts)

        # a = e1 / (e1 / subkw["E_1"])
        new_e1 = subkw["E_1"]
        new_e2 = subkw["E_1"] * (e1 / e2)
        subkw.update(E_1=new_e1, E_2=new_e2)

        # Re-testing
        field1_test = sym.lambdify(T, FIELD01_CPHI.subs(subkw))
        field2_test = sym.lambdify(T, FIELD02_CPHI.subs(subkw))

        e1t = fieldstrength(field1_test, ts)
        e2t = fieldstrength(field2_test, ts)

        tqdm.write(
            f"IntCorr // E1: {e1:.3e}, E1t: {e1t:.3e}\nE2: {e2:.3e}, E2t: {e2t:.3e}\n"
        )

        symfields = FIELD01_CPHI, FIELD02_CPHI

    elif not uphi and correct_intensity:
        tqdm.write(
            "Invalid or redundant specification Use_Phi: {}; CorrIntensity {}\n".format(
                uphi, correct_intensity
            )
        )
        return

    else:
        field1_phi = sym.lambdify(T, FIELD01_CPHI.subs(subkw))
        field2_phi = sym.lambdify(T, FIELD02_CPHI.subs(subkw))

        e1 = fieldstrength(field1_phi, ts)
        e2 = fieldstrength(field2_phi, ts)

        # a = e1 / (e1 / subkw["E_1"])
        # subkw.update(E_1=subkw["E_1"], E_2=subkw["E_2"])

        # Re-testing
        field1_test = sym.lambdify(T, FIELD01_CPHI.subs(subkw))
        field2_test = sym.lambdify(T, FIELD02_CPHI.subs(subkw))

        e1t = fieldstrength(field1_test, ts)
        e2t = fieldstrength(field2_test, ts)

        tqdm.write(f"FullSys // E1: {e1:.3e}, E1t: {e1t:.3e}\nE2: {e2:.3e}, E2t: {e2t:.3e}\n")

        symfields = FIELD01_CPHI, FIELD02_CPHI

    # Combined propagators -> RWA
    exp_field01_dt1 = sym.exp(
        -1j
        * DT1
        * Matrix([
            [0, -MU1 * symfields[0], 0],
            [-MU1 * symfields[0], 0, 0],
            [0, 0, 0],
        ])
    )

    exp_field02_dt2 = sym.exp(
        -1j
        * DT2
        * Matrix([
            [0, 0, -MU2 * symfields[1]],
            [0, 0, 0],
            [-MU2 * symfields[1], 0, 0],
        ])
    )

    fct = (
        exp_prop_dt4
        * exp_field02_dt2
        * exp_prop_dt4
        * exp_field01_dt1
        * exp_prop_dt4
        * exp_field02_dt2
        * exp_prop_dt4
    )

    # Generate the propagator function
    insfct = fct.subs(subkw)
    f = numba.njit(sym.lambdify(T, insfct, "numpy"))

    # Generate a starting wave function.
    wf = np.zeros((3, len(ts)), dtype="complex128")
    wf[0, 0] = 1.0

    # Progagation
    for idx, tt in enumerate(tqdm(ts[:], disable=disable_tqdm)):
        if idx > 0:

            prop = f(tt)
            wf[:, idx] = np.matmul(prop, wf[:, idx - 1])

#             if idx % 1000 == 0:
#                 tqdm.write(
#                     str(
#                         np.sum(wf[:, idx] * np.conj(wf[:, idx]))
#                     )
#                 )
        else:
            pass

    field1 = sym.lambdify(T, symfields[0].subs(subkw))
    field2 = sym.lambdify(T, symfields[1].subs(subkw))
    fields = np.stack((field1(ts), field2(ts)), axis=0)

    calc = Calculation(
        tgrid=ts,
        wf=wf,
        efield=fields,
        phi=subkw["phi"],
        epsilon=subkw["epsilon"],
        omega=subkw["omega"],
        J=subkw["J"],
        E1=subkw["E_1"],
        E2=subkw["E_2"],
        mu1=subkw["mu_1"],
        mu2=subkw["mu_2"],
        RWA=False,
        uphi=uphi,
        intcorr=correct_intensity,
        tau1=subkw["tau_m"],
        tau2=subkw["tau_p"],
        tau_coupled=adiab,
        off_resonant=off_resonant

    )
    final_path = calc.to_h5(outpath)

    return final_path


def main(debug=False):

    cname = "Phi0.5_1E05_Var-JMuAdiabTau"
    date = datetime.date.today().isoformat()
    outpath = Path(f"/data/eQD/{date}_{cname}/").expanduser()

    disable_tqdm = False

    print(outpath)

    ts, dtv = np.linspace(0, 39900, num=200000, retstep=True)

    subkwargs = {
        "dt_1": dtv,
        "dt_2": dtv / 2,
        "dt_3": dtv / 4,
        "dt_4": dtv / 8,
        "phi": float(0.5 * sym.pi),
        "epsilon": None,
        "omega": 0.10,
        "V": 0.1,
        "J": None,
        "mu_1": 1.0,
        "mu_2": None,
        "E_1": 1e-5,
        "E_2": 1e-5,
        "tau_m": None,
        "tau_p": None,
    }

    # Variable input parameters.
    tau_ms = np.array([215, 1E12], dtype="float64") / 0.024
    tau_ps = np.array([215, 1E12], dtype="float64") / 0.024

    adiabatics = [True]
    off_resonants = [True, False]
    js = [0.0025, -0.0025]
    epss = np.arange(11) * 0.01 - 0.1
    # epss = [-0.1]
    print("Eps:", np.array(epss))

    intensity_corrections = [True, False]
    use_phi = [True, False]
    mu2s = [1.0, -1.0]

    list_of_kws = []
    for tau_m, tau_p, adiab, off_resonant, j, eps, mu2, uphi, correct_intensity in it.product(
        tau_ms, tau_ps, adiabatics, off_resonants, js, epss, mu2s, use_phi, intensity_corrections
    ):

        if subkwargs["mu_1"] == mu2:
            aop = add if not off_resonant else sub

        elif subkwargs["mu_1"] == -mu2:
            aop = sub if not off_resonant else add

        else:
            raise ValueError("Did not get either the same or the opposite of mu1.")
            # aop = lambda x,y: x

        tqdm.write(
            f"Using omega '{aop.__name__}' J due to "
            f"mu2 = {mu2} and {('off-' if off_resonant else '')}resonant excitation"
        )

        # Update the submission kwargs
        subkw = {**subkwargs, **{
            "tau_m": tau_m,
            "tau_p": tau_p,
            "mu_2": mu2,
            "J": j,
            "epsilon": float(eps * sym.pi),
            "omega": aop(subkwargs["omega"], j),
            "opts": {
                "outpath": outpath,
                "tgrid": ts,
                "use_phi": uphi,
                "intcorr": correct_intensity,
                "adiab": adiab,
                "off_resonant": off_resonant,
            },
        }}

        list_of_kws.append(subkw)

    print(f"Number of jobs to be run: {len(list_of_kws)}")

    if debug:
        # Debug initialisation
        import pdb
        pdb.set_trace()

        # Instead of spawning multiple threads, use a standard for loop.
        for subkw in list_of_kws:
            integrator(subkw, disable_tqdm=False)

    else:
        with multiprocessing.Pool(multiprocessing.cpu_count() - 1) as pool:
            pool.map(partial(integrator, disable_tqdm=disable_tqdm), list_of_kws)


if __name__ == "__main__":
    main()