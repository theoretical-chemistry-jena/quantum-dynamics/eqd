"""
General utilities for the eQD package.

S.D.G.
"""

import datetime
import logging
from typing import Callable, Dict, TYPE_CHECKING, Tuple, Union, Optional

import joblib
import numba
import numpy as np
import scipy.linalg as sla
import sympy as sym
from tqdm import tqdm

from .symbols import T, W
from .types import SympyExpression

# https://adamj.eu/tech/2021/05/13/python-type-hints-how-to-fix-circular-imports/
if TYPE_CHECKING:
    from .calculation import Calculation


# Get the logger
logger = logging.getLogger(name=__name__)


class TqdmStream:
    """
    Basic tqdm stream object to use logging with tqdm.

    Only has a single classmethod `.write()` that streams a message to `tqdm.write()`.
    """

    @classmethod
    def write(cls, msg: str) -> None:
        """Stream message to tqdm.write

        Args:
            msg (str): The streamed message
        """

        tqdm.write(msg, end="")


class ProgressParallel(joblib.Parallel):
    """
    Custom tqdm-enabled subclass of the joblib.Parallel class.
    Based on https://stackoverflow.com/a/61900501/9940397
    """

    def __init__(
        self, *args, use_tqdm: bool = True, total: Optional[int] = None, **kwargs
    ):
        """Initialize a parallel progress bar.

        Args:
            use_tqdm (bool, optional):
                Whether to use tqdm at all. Defaults to True.
            total (Optional[int], optional):
                Total number of tasks to run through joblib.Parallel. Defaults to None.
        """
        # tqdm-related kwargs
        self._use_tqdm = use_tqdm
        self._total = total

        # This will be initialized later.
        self._pbar: Optional[tqdm] = None

        super().__init__(*args, **kwargs)

    def __call__(self, *args, **kwargs):
        with tqdm(
            disable=not self._use_tqdm, total=self._total, dynamic_ncols=True
        ) as self._pbar:
            return joblib.Parallel.__call__(self, *args, **kwargs)

    def print_progress(self):
        """
        This method will be called by the parallel callback (BatchCompletionCallBack).
        Therefore we can safely assume that the self._pbar is already populated.
        """

        if self._total is None:
            self._pbar.total = self.n_dispatched_tasks

        self._pbar.n = self.n_completed_tasks
        self._pbar.refresh()


def get_formatted_today(with_hours: bool = False) -> str:
    """Get a formatted string with the current date or datetime, for filenames.

    Args:
        with_hours (bool, optional): Also include hours and minutes in the string.
            Defaults to False.

    Returns:
        str: ISO-(ish) formatted date(time) string.
    """

    if not with_hours:
        date = datetime.date.today().isoformat()
    else:
        date = (
            datetime.datetime.now()
            .isoformat(sep="_", timespec="minutes")  # Get time to the minutes
            .replace(":", "-")  # Colons are very suboptimal for paths
        )

    return date


def mathify(string: str) -> str:
    """
    Utility for matplotlib plotting. Wraps a string in \\mathregular for better
    fonts in MPL plots.

    Args:
        string (str): String to be wrapped. Shoud not contain any '$' signs.

    Returns:
        str: Wrapped string.
    """
    return r"$\mathregular{{ {} }}$".format(string)


def fieldstrength_sympy(lambdified: Callable, time: Union[float, np.ndarray]):
    """
    Get the mean fieldstrength of a lambdified sympy field.

    Args:
        lambdified (Callable): Lambdified sympy expression. Should only depend on
            time.
        time (Union[float, np.ndarray]): Time array in which or time point at which
            to evaluate the fieldstrength.

    Returns:
        float: Fieldstrength
    """
    return np.sqrt(np.sum(np.abs(lambdified(time) ** 2)) / (time[-1] - time[0]))


def amplitude_combined_analyt(e_1: float, e_2: float, phi: float) -> float:
    r"""Calculate the analytical amplitude of a sum field $E_S = E_1 + E_2$
    where

    $$ E_1 = E_{0, 1} \cdot \cos(\omega t)$$

    and

    $$ E_2 = E_{0, 2} \cdot \cos(\omega t + \phi)$$

    Args:
        e_1 (float): Amplitude of the first field.
        e_2 (float): Amplitude of the second field.
        phi (float): Time-independent phase of the second field.

    Returns:
        float: The time-independent amplitude of the combined field.

    """
    return np.sqrt(e_1 ** 2 + e_2 ** 2 + 2 * e_1 * e_2 * np.cos(phi))


def phase_analyt(e_1, e_2, phi):
    r"""
    Calculate the analytical phase of a sum field $E_S = E_1 + E_2$
    where

    $$ E_1 = E_{0, 1} \cdot \cos(\omega t)$$

    and

    $$ E_2 = E_{0, 2} \cdot \cos(\omega t + \phi)$$

    Args:
        e_1 (float): Amplitude of the first field.
        e_2 (float): Amplitude of the second field.
        phi (float): Time-independent phase of the second field.

    Returns:
        float: The time-independent phase of the combined field.
    """
    return np.arcsin((e_2 / amplitude_combined_analyt(e_1, e_2, phi)) * np.sin(phi))


def combined_field(amplitude: float, phase: float) -> SympyExpression:
    """Return the Sympy expression of a generic electric field."""
    return amplitude * sym.cos(W * T + phase)


def get_field_with_effect(calc: "Calculation") -> Tuple[SympyExpression]:
    """
    Adjust field strengths to match a specific subsystem case, in the case of
    two excited states.

    Args:
        calc (Calculation): Calculation object to generate the fields for.

    Raises:
        NotImplementedError: Raised if the illegal combination "uphi: False, intcorr: True"
            is provided.
    Returns:
        Tuple[SympyExpression]: Tuple of the field
            expressions of length N (nr_monomers).
    """

    # Field specification
    system = calc.system

    # Get the spatially dependent quantities
    sp_dep_amplitudes = calc.np_amplitudes
    sp_dep_phases = calc.np_phases

    # Calculate the combined field amplitudes and phases
    amplitudes = [
        amplitude_combined_analyt(calc.E0, ampl, phase)
        for ampl, phase in zip(sp_dep_amplitudes, sp_dep_phases)
    ]
    phases = [
        phase_analyt(calc.E0, ampl, phase)
        for ampl, phase in zip(sp_dep_amplitudes, sp_dep_phases)
    ]

    # Intensity correction --> Phase changes, but amplitude does not.
    if system == "ic":
        fields = [combined_field(amplitudes[0], phase) for phase in phases]

    # Phase correction --> Amplitude changes, but phase does not.
    elif system == "pc":
        fields = [combined_field(ampl, phases[0]) for ampl in amplitudes]

    # Full system fallback
    else:
        fields = [
            combined_field(ampl, phase) for ampl, phase in zip(amplitudes, phases)
        ]

    return fields


def generate_fields_n(calc: "Calculation") -> Tuple[Tuple[SympyExpression], Dict]:
    """
    Generate and (based upon the information in the input) apply approximations to the field,
    such as intensity and phase correction. Currently these corrections are not implemented for
    models with $n_s \\ne 2$.

    Args:
        calc (Calculation): Calculation class to generate the fields for.

    Raises:
        NotImplementedError: If `calc.n_states != 2` and adjustments are requested.

    Returns:
        Tuple[Tuple[SympyExpression], Dict]:
            First item is a tuple of the SymPy fields,second is the (possibly empty)
            update dictionary to update the substitution kwargs with.
    """

    fields = get_field_with_effect(calc)

    if calc.pulsed:
        envelope = envelope_exp2(calc.sigma, calc.t_central)
        fields = [envelope * field for field in fields]

    logger.debug(fields)

    return tuple(fields)


def matmul3(a: np.ndarray, b: np.ndarray) -> np.ndarray:
    """
    Low-level BLAS-based function to calculate a matrix chain of three elements like
    $C = ABA$ with square (n x n) matrices A, B. Uses `cgemm` internally

    Args:
        a (np.ndarray): Matrix A (n x n), complex128.
        b (np.ndarray): Matrix B (n x n), complex128.

    Returns:
        np.ndarray: Matrix chain multiplication product $C = ABA$
    """
    c = sla.blas.cgemm(1.0, sla.blas.cgemm(1.0, a, b), a)  # pylint: disable=no-member
    return c


@numba.njit("c16[:,:](i8,c16[:,:],c16[:,:])")
def matmul3_numba(n: int, a: np.ndarray, b: np.ndarray) -> np.ndarray:
    """
    Numba-enabled function to calculate a matrix chain of three elements like
    $C = ABA$ with square (n x n) matrices A, B.

    TODO: Enable matrices with reals

    Args:
        n (int): Length $n$ of the n x n matrix.
        a (np.ndarray): Matrix A (n x n), complex128.
        b (np.ndarray): Matrix B (n x n), complex128.

    Returns:
        np.ndarray: Matrix chain multiplication product $C = ABA$
    """
    assert a.shape[0] == n
    assert a.shape[1] == n
    assert b.shape[0] == n
    assert b.shape[1] == n

    res = np.zeros_like(a, dtype=a.dtype)

    for i in range(n):
        for l in range(n):
            acc = 0
            for j in range(n):
                for k in range(n):
                    acc += a[i, j] * b[j, k] * a[k, l]

            res[i, l] = acc

    return res


def gen_hamiltonian_sym(n_states: int, V: float, J: float) -> sym.Matrix:
    """
    Generate a SymPy-Matrix Hamiltonian for use with eQD.
    This currently does not implement C coupling!

    Args:
        n_states (int): Number of states in the Hamiltonian
        V (float): Potential energy of the excited states
        J (float): Coupling energy between the excited states

    Returns:
        sym.Matrix: The Hamiltonian
    """
    hamiltonian = sym.zeros(rows=n_states, cols=n_states)

    for idx in range(1, n_states):
        hamiltonian[idx, idx] = V

        if idx > 1:
            hamiltonian[idx - 1, idx] = hamiltonian[idx, idx - 1] = J

    return hamiltonian


def analytical_diag(
    n_monomers: int, V: float, J: float
) -> Tuple[sym.Matrix, sym.Matrix]:
    """
    Return consistent Sympy-based diagonalization to rule out possible "root flips".
    Values are generated for a situation where J < V!

    Args:
        n_monomers (int): Number of monomers, most of the times number of states - 1.
        V (float): Diabatic excited state energy
        J (float): Diabatic inter-excited-state dipole coupling

    Raises:
        NotImplementedError: Currently, the implementation only covers 2M- and 4M-systems

    Returns:
        Tuple[sym.Matrix, sym.Matrix]: Tuple of transformation matrix and Hamiltonian
    """

    if not n_monomers in [2, 4]:
        raise NotImplementedError(
            "Currently, only 2- and 4-monomer systems support adiabatization"
        )

    # These values are taken from SymPy and can be calculated analytically
    # As they are not dependent on the values of V or J, we can just assume them here.

    if n_monomers == 4:

        u1 = 0.371748034460185
        u2 = 0.601500955007546
        s_5 = sym.sqrt(5)

        umat = sym.Matrix(
            [
                [1.0, 0.0, 0.0, 0.0, 0.0],
                [0.0, -u1, -u2, -u2, -u1],
                [0.0, -u2, -u1, +u1, +u2],
                [0.0, -u2, +u1, +u1, -u2],
                [0.0, -u1, +u2, -u2, +u1],
            ]
        )

        ham = sym.Matrix(
            [
                [0, 0, 0, 0, 0],
                [0, V + J / 2 + s_5 * J / 2, 0, 0, 0],
                [0, 0, V - J / 2 + s_5 * J / 2, 0, 0],
                [0, 0, 0, V - s_5 * J / 2 + J / 2, 0],
                [0, 0, 0, 0, V - s_5 * J / 2 - J / 2],
            ]
        )

    elif n_monomers == 2:
        u1 = sym.sqrt(2) / 2

        umat = sym.Matrix(
            [
                [1.0, 0.0, 0.0],
                [0.0, +u1, -u1],
                [0.0, +u1, +u1],
            ]
        )

        ham = sym.Matrix([[0, 0, 0], [0, V + J, 0], [0, 0, V - J]])

    else:
        raise NotImplementedError(
            "Currently, only 2- and 4-dimensional hamiltonians can be analytically diagonalized"
        )

    return umat, ham


# @numba.jit()
def envelope_cos2(period: float, t_central: float) -> sym.Piecewise:
    """Cosine-square envelope function for numerical fields. $f(t_{central}) = 1.0$.
    Despite being valid, this currently fails in the method described here due to
    Numba not supporting `.reduce` methods in numpy ufuncs.
    See https://github.com/numba/numba/issues/4504 and https://github.com/numba/numba/issues/4625.

    Args:
        t (Union[float, npt.ArrayLike]): Float or 1D numpy array of time.
        period (float): Width of the cosine pulse, "from 0 to 0". Corresponds to $1\\pi$ rad.
        t_central (float): Central point in time of the pulse.

    Returns:
        [Union[float, npt.ArrayLike]: 1D array (potentially (1,) shape) with the
        envelope amplitude.
    """
    f = sym.cos((sym.pi * (T - t_central)) / period) ** 2

    return sym.Piecewise(
        (f, (((t_central - period / 2) <= T) & (T <= (t_central + period / 2)))),
        (0, True),
    )


def envelope_exp2(sigma, t_central) -> SympyExpression:
    """Gaussian envelope function, for uses with Sympy models.

    Args:
        sigma (float): Standard deviation of the Gaussian function, controls the width.
        t_central (float): Central time point, arrival time of the pulse.

    Returns:
        SympyExpression: The Sympy expression corresponding to the envelope.
    """
    return sym.exp(-((T - t_central) ** 2) / (2 * sigma ** 2))
