"""
Propagators for the eQD package.

S.D.G.
"""

import logging

from dataclasses import dataclass
from typing import Any, Iterable, List, Optional, Tuple

import numba
import numpy as np
import sympy as sym

from sympy.matrices import Matrix
from sympy.utilities.lambdify import lambdastr
from sympy.printing.numpy import NumPyPrinter

from .calculation import Calculation
from .types import PropagatorFunctionT, SympyExpression
from .utils import (
    generate_fields_n as generate_fields,
    analytical_diag,
    matmul3_numba,
)
from .symbols import T


logger = logging.getLogger(name=__name__)
logging.getLogger(name="numba.core").setLevel(logging.ERROR)

#
# * Helper dataclasses
#


@dataclass(unsafe_hash=True)
class Propagator:
    """
    Helper dataclass to hold a propagator function and its respective split.

    Attributes:
        split (int):
            The split of the Propagator in the simulation
        lambdified (PropagatorFunctionT):
            Function of a single argument (t, the simulation time) that returns a
            np.ndarray with shape (n_states, n_states).
    """

    split: int
    lambdified: PropagatorFunctionT

    def __call__(self, t: float) -> np.ndarray:
        return self.lambdified(t)


@dataclass
class HamiltonianData:
    """Contains information on the fieldfree Hamiltonian used.

    Attributed:
        umat (np.ndarray):
            Unitary transformation matrix (eigenvectors of the Hamiltonian along the columns)
            with shape(n_states, n_states)
        ham_diab (np.ndarray):
            Diabatic Hamiltonian with shape(n_states, n_states)
        ham_adiab (np.ndarray):
            Adiabatic Hamiltonian with shape(n_states, n_states). Eigenvalues of the diabatic
            Hamiltonian are along the diagonals
    """

    umat: np.ndarray
    ham_diab: np.ndarray
    ham_adiab: np.ndarray


@dataclass
class PropagatorContainer:
    """
    Contains objects used for the propagation.

    Attributes:
        props (Iterable[Propagator]):
            The propagators used for the propagation in a sorted iterable.
        fieldfuncs (Iterable[SympyExpression]):
            The SymPy expressions that build the time-dependent electric fields.
        hamiltonian_data (HamiltonianData):
            The information about the fieldfree Hamiltonian.
    """

    props: Iterable[Propagator]
    fieldfuncs: Iterable[SympyExpression]
    hamiltonian_data: HamiltonianData


def field_prop(calc: Calculation, split: int, idx: int, field: Any) -> Matrix:
    """
    Generate an electric field propagator from the information given

    Args:
        calc (Calculation): The calculation object holding the required parameters.
        split (int): The split that is currently required
        idx (int): The index of the final state that will be coupled to from the GS.
        field (Any): Sympy expression that holds the field equation.

    Returns:
        Matrix: Sympy matrix of the time-dependent potential energy Hamiltonian
    """

    dt = calc.dt_split(split)

    # We need an identity matrix here, instead of a zero matrix.
    mat = calc.get_identity_propagator_matrix()

    # Sympy appears confused when we use an envelope function. Therefore,
    # we need to specify the pair propagator manually (cos/sin terms)
    mat[0, 0] = sym.cos(dt * -calc.mus[idx - 1] * field)
    mat[idx, idx] = sym.cos(dt * -calc.mus[idx - 1] * field)
    mat[idx, 0] = -1.0j * sym.sin(dt * -calc.mus[idx - 1] * field)
    mat[0, idx] = -1.0j * sym.sin(dt * -calc.mus[idx - 1] * field)

    logger.debug(repr(mat))

    return mat


def ti_hamiltonian_prop(
    calc: Calculation, split: int
) -> Tuple[Matrix, HamiltonianData]:
    """
    Generate the time-independent potential energy propagator

    Args:
        calc (Calculation): Calculation instance holding the necessary parameters.
        split (int): Integer split of the propagator

    Returns:
        Tuple[Matrix, HamiltonianData]:
            Sympy matrix of the time-independent potential energy Hamiltonian
    """

    # Generate the non-diagonal V+J matrix
    hamiltonian = calc.get_fieldfree_hamiltonian()

    # If we also include a nanoparticle state, populate the state with the
    # correct energy
    if calc.include_np_coupling:
        hamiltonian[-1, -1] = calc.V_np

        for idx in range(1, calc.n_monomers + 1):
            hamiltonian[-1, idx] = calc.C_np[idx - 1]
            hamiltonian[idx, -1] = calc.C_np[idx - 1]

    if calc.n_monomers in [2, 4]:
        umat, diag = analytical_diag(calc.n_monomers, calc.V, calc.J)
    else:
        umat, diag = hamiltonian.diagonalize()

    logger.debug("The Hamiltonian: \r\n{!r}".format(hamiltonian))
    logger.debug("The diagonalized Hamiltonian: \r\n{!r}".format(diag))
    logger.debug("The transform matrix: \r\n{!r}".format(umat))

    # This computes the exponential of the diagonal propagator
    # Define the propagator for the time-independent potential energy Ham.
    # https://en.wikipedia.org/wiki/Eigendecomposition_of_a_matrix#Functional_calculus
    prop = umat * sym.exp(-1j * calc.dt_split(split) * diag) * umat.T

    logger.debug("The Hamiltonian propagator: \r\n{!r}".format(prop))

    return (prop, HamiltonianData(umat=umat, ham_adiab=diag, ham_diab=hamiltonian))


def ti_damping_prop(
    calc: Calculation, split: int, umat: Optional[Matrix] = None
) -> Matrix:
    """
    Generate the time-independent potential energy propagator

    Args:
        calc (Calculation): Calculation instance holding the necessary parameters.
        split (int): Integer split of the propagator

    Raises:
        ValueError: For excitonic damping (damping in the adiabatic basis), we need the
            unitary transformation matrix here.
        ValueError: If the shape of the transformation matrix does not conform with the
            shape required by the Calculation object given.

    Returns:
        Matrix: Sympy matrix of the time-independent potential energy Hamiltonian
    """

    if calc.tau_coupled:
        if umat is None:
            raise ValueError(
                "If you specify excitonic damping, you have to specify the "
                "unitary transformation matrix."
            )
        elif umat.shape[0] != calc.n_totalstates or len(umat.shape) != 2:
            logger.debug(
                "Umat shape: {}, n_totalstates: {}".format(
                    umat.shape, calc.n_totalstates
                )
            )
            raise ValueError("Shape mismatch for the transformation matrix.")

    # Generate the diagonal igamma matrix
    igamma = calc.get_zero_propagator_matrix()
    for idx in range(1, calc.n_states):

        igamma[idx, idx] = -1.0j * (1.0 / (2.0 * float(calc.taus[idx - 1])))

    logger.debug("The Damping operator: \r\n{!r}".format(igamma))

    # If you want to damp in the excitonic picture, you need to
    # transform the damping operator or not.
    # Define the propagator for the time-independent potential energy Ham.
    if calc.tau_coupled:
        prop = umat * sym.exp(-1j * calc.dt_split(split) * igamma) * umat.T
    else:
        prop = sym.exp(-1j * calc.dt_split(split) * igamma)

    logger.debug("The Damping propagator: \r\n{!r}".format(prop))

    return prop


def fixed_jitted_lambdify(subbed_expr: SympyExpression) -> PropagatorFunctionT:
    """
    Workaround function to jit-compile a Sympy-generated lambda function.
    Due to a bug in Numba (that I yet need to report), standard jitting of the
    lambdified function fails.

    Args:
        subbed_expr (SympyExpression): The expression that has no symbol-dependencies
            besides for the time symbol 'T'. (It's also ok if it has none.)

    Returns:
        PropagatorFunctionT: Single-argument function of the lambdified expression.
    """

    # Fix the Numba-related issues with the lambda function as a string.
    str_lambda = lambdastr(T, subbed_expr, NumPyPrinter)

    # TODO : Get proper replacement with regular expressions
    try:
        str_lambda_repl = (
            str_lambda.replace("1.0,", "(1.0 + 0.0j),")
            .replace("1.0]", "(1.0 + 0.0j)],")
            .replace(" 0,", " (0.0 + 0.0j),")
            .replace("[0,", "[(0.0 + 0.0j),")
            .replace("0]", "(0.0 + 0.0j)]")
            .replace("numpy", "np")
        )

        # logger.debug(str_lambda_repl)

        # Then, eval the modded lambda function and JIT it.
        lambdified = numba.njit(eval(str_lambda_repl))  # pylint: disable=eval-used

    except SyntaxError as e:
        logger.error(
            "Got syntax error for lambdify fixing. Trying with bare string. "
            "I'll fix it soon. Error {}".format(e)
        )

        lambdified = numba.njit(
            eval(str_lambda.replace("numpy", "np"))  # pylint: disable=eval-used
        )

    # Do the first evaluation here directly.
    logger.debug(
        "First evaluation of the function @ t = 0.1:\r\n{!r}".format(lambdified(0.1))
    )

    return lambdified


def generate_props(calc: Calculation) -> PropagatorContainer:
    """
    Calculates the time-independent and all time-dependent propagators for the
    current calculation instance.

    Args:
        calc (Calculation): The calculation instance that holds the parameters.

    Returns:
        PropagatorContainer: An instance of the PropagatorContainer container that holds the
        jitted functions, field functions, the tranformation matrices and
        the diagonal Hamiltonian.
    """

    # subkw is what we will parse to Sympy for substitution later
    subkw = calc.subsdict

    # Get and potentially adjust the fieldstrength etc.
    # ! FIXME for more than two excited states.
    fields = generate_fields(calc)

    logger.debug("Got {} fields from the adjust_fields function.".format(len(fields)))

    props = []

    # * Build the time-dependent propagators
    for s_exp, field in enumerate(fields):
        split = 2 ** s_exp
        cmonomer_idx = s_exp + 1
        td_prop = field_prop(
            calc=calc, split=split, idx=cmonomer_idx, field=field
        ).subs(subkw)

        lambdified = fixed_jitted_lambdify(td_prop)

        # Then, add all the necessary info to a Propagator dataclass
        props.append(Propagator(split=split, lambdified=lambdified))

    ti_start_exp = len(fields)

    # * Add the time-independent propagators
    n_ti_props = 1
    n_ti_props += int(not calc.disable_damping)

    ti_splits = [2 ** (ti_start_exp + i) for i in range(n_ti_props)]

    logger.debug(
        "Time-independent splits: nr: {}, vals: {}".format(n_ti_props, ti_splits)
    )

    hamiltonian_prop, hamiltonian_data = ti_hamiltonian_prop(
        calc=calc, split=ti_splits[0]
    )

    tiprops = [hamiltonian_prop]
    if not calc.disable_damping:
        tiprops.append(
            ti_damping_prop(calc=calc, split=ti_splits[1], umat=hamiltonian_data.umat)
        )

    # Lambdify and jit the time-independent propagators
    for prop, split in zip(tiprops, ti_splits):
        lambdified = fixed_jitted_lambdify(prop)
        props.append(Propagator(split=split, lambdified=lambdified))

    return PropagatorContainer(
        props=props, fieldfuncs=fields, hamiltonian_data=hamiltonian_data
    )


def recurse_prop(
    n_states: int,
    split_exps: Iterable[int],
    funcs: Iterable[Propagator],
    split_exp: int = 0,
) -> PropagatorFunctionT:
    """
    Recursively build a callable propagator function that can be called to give the
    time-dependent propagator.

    Args:
        n_states (int): Number of states in the system.
        split_exps (Tuple[int]): All the split exponents used. (0, 1, 2, 3, ...) is eqv.
            to splits of (1, 2, 4, 8, ...).
        funcs (Tuple[Propagator]): A tuple of the Propagator instances in ascending split
            order. The len(funcs) == len(split_exps)
        split_exp (int): The current split exponent. Do not set it manually! Defaults to 0.

    Raises:
        ValueError: This should never be raised.
        It will be the case if splits are negative or the input is severely malformed.

    Returns:
        PropagatorFunctionT: Agglomerated function stack that only depends on time t.
    """

    # Bottom edge case
    if split_exp + 1 not in split_exps:
        logger.info("Called the bottom edge case")
        return lambda t: funcs[  # pylint: disable=unnecessary-lambda
            split_exp
        ].lambdified(t)

    # Bottom case
    elif split_exp + 1 in split_exps and split_exp + 2 not in split_exps:
        logger.info("Called the bottom case")
        return lambda t: matmul3_numba(
            n_states,
            funcs[split_exp + 1].lambdified(t),
            funcs[split_exp].lambdified(t),
        )

    # If we continue recursing
    elif split_exp + 1 in split_exps and split_exp + 2 in split_exps:
        logger.info("Called the recursive case")

        # The next recursion is called here, as
        lower_func = recurse_prop(n_states, split_exps, funcs, split_exp + 1)

        return lambda t: matmul3_numba(
            n_states,
            lower_func(t),
            funcs[split_exp].lambdified(t),
        )

    else:
        raise ValueError("Something in the split finding did not work out.")


class PropagatorAggregator:
    """
    Callable final propagator generator, for convenience.

    Attributes:
        props List[Propagator]:
            List of propagators used in the propagation, as generated
            in the `generate_props()` function.
        n_states (int):
            Number of states in the propagation, used for verification of
            shapes and sizes of the propagators.
    """

    def __init__(self, props: List[Propagator], n_states: int) -> None:
        """Propagator initializer and wrapper class.

        Args:
            props (List[Propagator]): A list of propagator instances.
            n_states (int): The number of states in the system.
        """
        # Given attributes
        self.props = tuple(sorted(props, key=lambda x: x.split))
        self.n_states: int = n_states

        # Initialize and compute the function
        self._func: PropagatorFunctionT = self._prepare_lambda()

    def _prepare_lambda(self):
        # exps are the split _exponents_ of the propagators
        exps = tuple(range(len(self.props)))
        return recurse_prop(self.n_states, exps, self.props)

    def _naive_compute(self, t: float) -> np.ndarray:
        """
        Private test function to assert equality of the calculated propagator.

        Args:
            t (float): Time in atomic units
        """

        if len(self.props) == 3:
            subprop = self.props[2](t) @ self.props[1](t) @ self.props[2](t)
            return (lambda t: subprop @ self.props[0](t) @ subprop)(t)

        elif len(self.props) == 4:
            subprop1 = self.props[3](t) @ self.props[2](t) @ self.props[3](t)
            subprop2 = subprop1 @ self.props[1](t) @ subprop1

            return (lambda t: subprop2 @ self.props[0](t) @ subprop2)(t)

        else:
            return NotImplementedError(
                "Naive computation is only implemented for 3 and 4 propagators"
            )

    def __call__(self, t: float) -> np.ndarray:
        return self._func(t)
