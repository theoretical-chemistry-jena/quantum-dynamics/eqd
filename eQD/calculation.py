"""
Calculation class and DBEntry representation

S.D.G.

"""

import json
import logging
import uuid

from dataclasses import dataclass, field
from pathlib import Path
from typing import Dict, Iterable, List, Optional, Union


import h5py
import numpy as np
import sympy as sym
from sympy.matrices import Matrix

from .utils import gen_hamiltonian_sym, get_formatted_today

logger = logging.getLogger(name=__name__)


@dataclass
class DBEntry:
    """Iterable database entry for Calculation classes

    Attributes:
        path: (Union[Path, str]): Absolute or relative path of the HDF5 file.

        phi (float): Phase in rad.

        epsilon (float): Spatial phase shift in rad.

        omega ([float]): Field frequency.

        J (float): Electronic coupling between excited states.

        mu (Union[Iterable[float], str]): Transition dipole moments.

        tau (Union[Iterable[float], str]): Damping coefficients.

        E (Union[Iterable[float], str]): Field strengths.

        disable_damping (Union[int, bool]): Whether to disable damping.

        include_np_coupling (Union[int, bool]): Flag to include NP coupling.

        tau_coupled (Union[int, bool]): Flag to indicate whether coupling is
            applied in diabatic or adiabatic regime.

        uphi (Union[int, bool]): Whether the explicit phase is used in the calc.

        intcorr (Union[int, bool]): Whether field amplitude/intensity correction
            was applied.
    """

    # Basics
    path: Union[Path, str] = field(metadata={"sqltype": "TEXT UNIQUE"})
    phi: float = field(metadata={"sqltype": "REAL"})
    epsilon: float = field(metadata={"sqltype": "REAL"})
    omega: float = field(metadata={"sqltype": "REAL"})
    J: float = field(metadata={"sqltype": "REAL"})
    E0: float = field(metadata={"sqltype": "REAL"})
    beta: float = field(metadata={"sqltype": "REAL"})
    eta: float = field(metadata={"sqltype": "REAL"})

    mu: Union[Iterable[float], str] = field(metadata={"sqltype": "JSON1"})

    # Options
    disable_damping: Union[int, bool] = field(metadata={"sqltype": "INTEGER"})
    include_np_coupling: Union[int, bool] = field(metadata={"sqltype": "INTEGER"})
    tau_coupled: Union[int, bool] = field(metadata={"sqltype": "INTEGER"})
    uphi: Union[int, bool] = field(metadata={"sqltype": "INTEGER"})
    intcorr: Union[int, bool] = field(metadata={"sqltype": "INTEGER"})

    # Optional parameters
    pulsed: bool = field(metadata={"sqltype": "INTEGER"})
    t_central: Optional[float] = field(metadata={"sqltype": "REAL"})
    sigma: Optional[float] = field(metadata={"sqltype": "REAL"})
    tau: Optional[Union[Iterable[float], str]] = field(metadata={"sqltype": "JSON1"})
    V_np: Optional[float] = field(metadata={"sqltype": "REAL"})
    C_np: Optional[Union[Iterable[float], str]] = field(metadata={"sqltype": "JSON1"})

    # Iterator, to be filled later from __dict__
    _iter: int = field(default=None, init=False)

    def __post_init__(self):
        # if self.V_np is None:
        for key in self.__dict__:
            if getattr(self, key) == "null":
                setattr(self, key, None)

    def __len__(self):
        return len(self.__dict__)

    def __iter__(self):
        self._iter = iter(self.__dict__.copy().keys())
        return self

    def __next__(self):
        try:
            key = next(self._iter)

            if key == "_iter":
                raise StopIteration

            val = self.__dict__[key]

            return val

        except IndexError as e:
            raise StopIteration from e

    @classmethod
    def get_db_init_query(cls, table_name: str = "data") -> str:
        """Get a SQL query for instantiating a data table

        Args:
            table_name (str): Name of the table to create. Defaults to "data".

        Returns:
            str: SQL query

        """
        fields = cls.__dataclass_fields__  # pylint: disable=no-member

        queries = []
        for dfield in fields.keys():
            if not dfield.startswith("_"):
                sqltype = fields[dfield].metadata["sqltype"]
                queries.append(f"{dfield} {sqltype}")

        full_query = "CREATE TABLE {} ({});".format(table_name, ", ".join(queries))

        logger.debug("Full init query: {}".format(full_query))

        return full_query

    def get_population_query(self):
        """Generate a SQLite query to populate a eQD.db database with.

        Returns:
            str: SQL query
        """
        query = "INSERT INTO data VALUES "
        query += "(" + ", ".join(["?"] * len(self)) + ")"

        return query


@dataclass(eq=True, order=True, repr=True, unsafe_hash=True)
class Calculation:
    """
    Container class that holds all the information required to setup, run and save
    an eQD simulation.

    Attributes:
        n_monomers (int):
            Number of monomers in the system. Defaults to 2.

        omega (float):
            Incident field frequency in hartree.

        phi (float):
            Stationary phase shift by the nanoparticle, in radians.

        epsilon (float):
            Spatially-dependent phase shift, in radians per monomer.

        V (float):
            Diabatic excited state energy of an uncoupled monomer, in hartree.

        J (float):
            Diabatic excited state inter-monomer electronic coupling energy, in hartree.

        uphi (bool):
            Whether to use explicit phase.

        intcorr (bool):
            Whether to apply intensity correction.

        E0 (float): Field strengths to be applied to each monomer.

        beta (float): Spatial-coordinate dependent field amplitude scaling factor.
            For the monomer $n$ ($n \\in [0, ..., N-1]$), $\\beta_{eff} = \\beta^n$.

        eta (float): NP-mediated field enhancement at $R=0$ (i.e., the position
            of the first monomer).

        mus (Iterable[float]):
            List of diabatic transition dipole moments to be applied to each monomer.
            Has to fullfill `len(mus) == n_monomers`.

        cname (str):
            User-defined, human-readable identifier of the calculation.

        outpath (Union[Path, str]):
            Path in which to save the calculation output file.

        tgrid (np.ndarray):
            Time point array. This is populated over the course of the population.
            User-given data will be overwritten.

        wf (np.ndarray):
            Wavefunction array. This is populated over the course of the population.
            User-given data will be overwritten.

        efield (np.ndarray):
            Field strength array. This is populated over the course of the population.
            User-given data will be overwritten.

        pot_diab (np.ndarray):
            Diabatic potential energy surface. This is populated over the course
            of the population. User-given data will be overwritten.

        pot_adiab (np.ndarray):
            Adiabatic potential energy surface. This is populated over the course
            of the population. User-given data will be overwritten.

        umat (np.ndarray):
            Unitary transformation matrix. This is populated over the course of
            the population. User-given data will be overwritten.

        umat_inv (np.ndarray):
            Inverse of  the unitary transformation matrix. This is populated over the
            course of the population. User-given data will be overwritten.

        dt (float):
            Time step in atomic units of time.

        include_np_coupling (bool):
            Whether to apply coupling to an external NP state. This behavior is
            currently broken.

        V_np (Optional[float]):
            Diabatic potential energy of the coupled nanoparticle state. Broken.

        C_np (Optional[float]):
            Diabatic excited state coupling energy of the coupled nanoparticle. Broken.

        pulsed (bool):
            Whether to specify a time-dependent envelope. Currently, there is only a Gaussian
            envelope function.

        t_central (Optional[float]):
            Maximum time of the Gaussian pulse. Only required if `pulsed == True`.

        sigma (Optional[float]):
            Standard deviation of the Gaussian pulse. Only required if `pulsed == True`.

        taus (Iterable[float]):
            List of decoherence lifetimes to be applied to each monomer.
            Has to fullfill `len(taus) == n_monomers`.

        tau_coupled (bool):
            Whether the decoherence lifetime `tau` is applied on the adiabatic (True)
            or diabatic (False) states.

        disable_damping (bool):
            Whether to disable decoherence altogether.
"""

    # Important parameters
    n_monomers: int

    omega: float
    phi: float
    epsilon: float
    V: float
    J: float
    uphi: bool
    intcorr: bool
    # Maximum field strength of the incident field
    E0: float
    mus: Iterable[float]

    # R-dependent field amplitude factor. beta**n for monomer n (in [0, ... N-1])
    beta: float = 1
    # Static field enhancement on the surface
    eta: float = 1

    # Output-related parameters
    cname: str = field(default_factory=get_formatted_today)
    outpath: Union[Path, str] = field(default=Path("."))

    # NDARRAYS
    ARRAY_KWARGS = dict(
        compare=False, repr=False, hash=False, metadata={"dataset": True}
    )

    tgrid: np.ndarray = field(default=None, **ARRAY_KWARGS)
    wf: np.ndarray = field(default=None, **ARRAY_KWARGS)
    efield: np.ndarray = field(default=None, **ARRAY_KWARGS)

    # Transformation matrices and adiabatic potential
    pot_diab: np.ndarray = field(default=None, **ARRAY_KWARGS)
    pot_adiab: np.ndarray = field(default=None, **ARRAY_KWARGS)
    umat: np.ndarray = field(default=None, **ARRAY_KWARGS)
    umat_inv: np.ndarray = field(default=None, **ARRAY_KWARGS)

    dt: float = field(compare=False, repr=False, default=None)

    ## Defaulting parameters
    # NP coupling-related. Currently broken
    include_np_coupling: bool = field(default=False)
    V_np: Optional[float] = field(default=None)
    C_np: Optional[float] = field(default=None)

    # Extra field specs
    pulsed: bool = field(default=False)
    t_central: float = field(default=None)
    sigma: float = field(default=None)

    # Damping parameters
    taus: Iterable[float] = field(default=None)
    tau_coupled: bool = field(default=False)

    _n_states: int = field(default=None)
    _n_totalstates: int = field(default=None)

    # Kill switch for damping
    disable_damping: bool = field(repr=None, compare=False, default=False)

    _filepath: Optional[Union[Path, str]] = field(
        repr=None, compare=False, default=None
    )
    _subsdict: Optional[Dict] = field(repr=None, compare=False, default=None)

    def __post_init__(self):
        if self.dt is None and self.tgrid is not None:
            self.dt = np.abs(self.tgrid[1] - self.tgrid[0])

        self._n_states = self.n_monomers + 1

        # Empty tuple evaluates to False
        if not self.taus and not self.disable_damping:
            logger.warning("Tau's were not specified, I assume you want it disabled.")
            self.disable_damping = True

        if (self.V_np is None or self.C_np is None) and self.include_np_coupling:
            logger.warning(
                "V_np or C_np were not specified, I assume you don't want to couple to a NP."
            )
            self.include_np_coupling = False

        if self.pulsed and (self.t_central is None or self.sigma is None):
            logger.warning(
                "You specified pulsed excitation of the system but did not specify "
                "'t_central' AND/OR 'sigma'. Pulse envelope will be set to f(t) = 1."
            )
            self.pulsed = False

        self._n_totalstates = self._n_states + int(self.include_np_coupling)

    @classmethod
    def from_file(cls, filepath: Union[Path, str]):
        """
        Synonymous to .from_h5, for convenience.
        """
        return cls.from_h5(filepath)

    @classmethod
    def from_h5(cls, filepath: Union[Path, str]):
        """
        Build a Calculation instance from a h5 file.

        Args:
            filepath (Union[Path, str]): Path to the h5 file to load the Calculation
                object from.

        Returns:
            Calculation: Recovered calculation object.
        """

        with h5py.File(filepath, "r") as f:
            attribs = dict(f.attrs)

            clean_attribs = {
                k: tuple(v) for k, v in attribs.items() if isinstance(v, np.ndarray)
            }
            attribs = {**attribs, **clean_attribs}

            for key in cls._get_dataset_attribs():
                attribs[key] = np.asarray(f[key])

        return cls(_filepath=filepath, **attribs)

    @classmethod
    def _get_field_metadata(cls, key) -> Dict:
        return cls.__dataclass_fields__[key].metadata  # pylint: disable=no-member

    @classmethod
    def _get_dataset_attribs(cls):
        filterfunc = lambda x: "dataset" in cls._get_field_metadata(x)
        return list(
            filter(filterfunc, cls.__dataclass_fields__)  # pylint: disable=no-member
        )

    @classmethod
    def _get_public_attribs(cls, extra_excludes: Optional[List[str]] = None):

        if extra_excludes is None:
            extra_excludes = []

        excluded_attribs = [*cls._get_dataset_attribs(), *extra_excludes]
        filterfunc = lambda x: x not in excluded_attribs and x[0] != "_"

        public_attribs = list(
            filter(filterfunc, cls.__dataclass_fields__) # pylint: disable=no-member
        )

        return public_attribs

    @property
    def system(self) -> str:
        """Return the approximation system string.

        Returns:
            str: One of "full" (normal system),
                "ic" (amplitude identical for all monomer units),
                "pc" (phase identical for all monomer units).

        Raises:
            KeyError: If the illegal combination is specified in the Calculation.
        """
        return {
            (True, False): "full",
            (True, True): "ic",
            (False, False): "pc",
        }[(self.uphi, self.intcorr)]

    @property
    def np_amplitudes(self) -> List[float]:
        """The spatially dependent amplitudes of the nanoparticle-emitted field"""
        return [
            (self.beta ** (n) * self.eta * self.E0) for n in range(self.n_monomers)
        ]

    @property
    def np_phases(self) -> List[float]:
        """The spatially dependent amplitudes of the nanoparticle-emitted field"""
        return [self.phi + n * self.epsilon for n in range(self.n_monomers)]

    @property
    def subsdict(self) -> Dict:
        """
        Get the SymPy-formatted substitution dict.

        Returns:
            dict: Mapping of SymPy symbol names to values.
        """
        if not self._subsdict:

            ldict = {}
            for item in ["mus", "taus"]:
                val = getattr(self, item)
                if val is not None:
                    for idx, val in enumerate(val):
                        lbl = f"{item[:-1]}_{idx + 1}"
                        ldict[lbl] = val

            self._subsdict = {
                "dt": self.dt,
                "phi": self.phi,
                "epsilon": self.epsilon,
                "omega": self.omega,
                "V": self.V,
                "J": self.J,
                "beta": self.beta,
                "eta": self.eta,
                "E0": self.E0,
                **ldict,
            }

            logger.debug("The calculation.subsdict: {!r}".format(self._subsdict))

        return self._subsdict

    @property
    def n_states(self):
        """
        Get the number of states in the system.
        Usually `self.n_monomers + 1`.
        """
        return self._n_states

    @property
    def n_totalstates(self):
        """
        Get the total states of the system.
        This might include a NP-localized state.
        Currently, this is equivalent to self.n_states.
        """
        return self._n_totalstates

    def get_zero_propagator_matrix(self) -> Matrix:
        """
        Based on the information given, return a sympy matrix with the correct
        number of total states.

        Returns:
            sympy.Matrix: Matrix of zeros with the shape necessary to use it for
            propagators.
        """
        return sym.matrices.zeros(self._n_totalstates, self._n_totalstates)


    def get_identity_propagator_matrix(self) -> Matrix:
        """
        Based on the information given, return a sympy matrix with the correct
        number of total states.

        Returns:
            sympy.Matrix: Matrix of zeros with the shape necessary to use it for
            propagators.
        """
        return sym.matrices.eye(self._n_totalstates)


    def get_fieldfree_hamiltonian(self) -> Matrix:
        """Generate a fieldfree Hamiltonian that suits the needs for the
        calculation instance at hand.

        Returns:
            sympy.Matrix: Field-free Hamiltonian with shape (n_states, n_states).
        """
        return gen_hamiltonian_sym(self._n_states, self.V, self.J)

    def dt_split(self, split: int) -> float:
        """Convenience method for generating a split timestep from the original timestep.

        Args:
            split (int): Divisor, has to be a positive integer.

        Returns:
            float: Split timestep.
        """
        if not isinstance(split, int):
            raise TypeError("Split has to be an integer.")

        if split <= 0:
            raise ValueError("Split has to be non-zero and positive.")

        return self.dt / split

    def _check_create_output_folder(self):
        folder = Path(self.outpath).expanduser().resolve()

        pth = Path(folder).resolve()
        if not pth.exists():
            pth.mkdir(parents=True)

        return pth

    def _clear_arrays(self):
        del self.tgrid
        del self.wf
        del self.efield
        del self.umat
        del self.umat_inv
        del self.pot_diab
        del self.pot_adiab

    def to_h5(self, stem: Optional[str] = None, remove_arrays: bool = False) -> Path:
        """Output the calculation data to HDF5 data. This method sets the self._filepath.

        Args:
            stem (Optional[str], optional): Custom filename stem. By default, a random
                UUID will be used. Defaults to None.
            remove_arrays (bool): Whether to free all the allocated array attributes
                after outputting. Defaults to False.

        Raises:
            FileExistsError: In the case that the file already exists.

        Returns:
            Path: Path under which the file can be found.
        """
        # Use the outpath from the input

        if stem is None:
            # Generate a random UUID
            stem = str(uuid.uuid4())

        pth = self._check_create_output_folder()
        fname = ".".join((self.cname, stem, "h5"))
        fpath = pth / fname

        logger.debug("Dataset attributes: {!r}".format(self._get_dataset_attribs()))
        logger.debug("Public attributes: {!r}".format(self._get_public_attribs()))

        try:
            with h5py.File(fpath, "w-") as h5f:

                for dset in self._get_dataset_attribs():
                    h5f.create_dataset(dset, data=getattr(self, dset))

                for k in self._get_public_attribs(extra_excludes=["outpath"]):

                    val = getattr(self, k)

                    if not val is None:
                        logger.debug(
                            "Adding attribute {k} with value {v}".format(k=k, v=val)
                        )

                        h5f.attrs[k] = val

        # Fallback in case UUID creation yiels
        except FileExistsError as e:
            if stem is None:
                logger.warning(
                    "The almost impossible happened: A double UUID was issued for the file "
                    "name creation and the file data was not saved. "
                    "I'll keep running nonetheless."
                )
            else:
                raise FileExistsError(
                    "A file with the same name was already found. Exiting."
                ) from e

        self._filepath = fpath

        logger.info("File written to {}".format(fpath))

        if remove_arrays:
            self._clear_arrays()

        return fpath

    @property
    def dbentry(self) -> DBEntry:
        """Return the DBEntry representation of the instance.

        Raises:
            ValueError: If the calculation has not been saved to a file, then
                exporting to the database cannot succeed.

        Returns:
            DBEntry: DBEntry representation of the calculation inputs.
        """

        if self._filepath is None:
            raise ValueError(
                "The class needs to be exported to a suitable filepath "
                "before exporting to a database."
            )

        return DBEntry(
            # Required params
            path=str(self._filepath),
            phi=self.phi,
            epsilon=self.epsilon,
            omega=self.omega,
            J=self.J,
            uphi=int(self.uphi),
            intcorr=int(self.intcorr),
            mu=json.dumps(self.mus),
            E0=self.E0,
            beta=self.beta,
            eta=self.eta,

            # Damping-related
            disable_damping=int(self.disable_damping),
            tau_coupled=int(self.tau_coupled),
            tau=json.dumps(self.taus),

            # Pulse-related
            pulsed=int(self.pulsed),
            t_central=self.t_central,
            sigma=self.sigma,

            # NP coupling
            include_np_coupling=int(self.include_np_coupling),
            C_np=json.dumps(self.C_np),
            V_np=self.V_np,
        )
