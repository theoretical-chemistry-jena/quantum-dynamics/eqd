""""
Main integrator module that does the actual propagation.

S.D.G.
"""

import logging
from pathlib import Path

import numpy as np
import sympy as sym

from .calculation import Calculation
from .propagators import generate_props, PropagatorAggregator

from .symbols import T


# Get the logger
logger = logging.getLogger(name=__name__)


def integrator(
    calc: Calculation, return_calc: bool = False, keep_arrays: bool = True
) -> Path:
    """
    Main integrator function. In the process of the propagation,
    writes information back to the Calculation instance and in the case of
    output information already being there, will overwrite the old data.

    Args:
        calc (Calculation): Pre-initialized Calculation instance.
        return_calc (bool): Whether to return the calc object after calculation.
            Required in parallel context. Defaults to False.
        keep_arrays (bool): Whether to keep the Calculation's arrays after the simulation.
            Otherwise, array

    Returns:
        Path: Filepath of the resulting .h5 file.
    """

    tgrid = calc.tgrid

    propdatacontainer = generate_props(calc)
    prop_aggregator = PropagatorAggregator(propdatacontainer.props, calc.n_totalstates)

    debug_msg = "Idx: {idx:8d}; Full norm: {full:.6f}; Single norms: {single}"

    # * Generate a starting wave function.
    wf = np.zeros((calc.n_totalstates, len(tgrid)), dtype="complex128")
    wf[0, 0] = 1.0

    # * The actual progagation
    for idx, tt in enumerate(tgrid):
        if idx > 0:

            prop = prop_aggregator(tt)
            wf[:, idx] = np.matmul(prop, wf[:, idx - 1])

            if idx % 1000 == 0 and logger.level <= logging.DEBUG:
                fnorm = np.real(np.sum(wf[:, idx] * np.conj(wf[:, idx])))
                singles = ", ".join(
                    [
                        "{:.6f}".format(np.real(wf[i, idx] * np.conj(wf[i, idx])))
                        for i in range(calc.n_states)
                    ]
                )
                logger.debug(debug_msg.format(idx=idx, full=fnorm, single=singles))
        else:
            pass

    # * Output/postprocessing phase
    # Output fields
    fieldslist = []
    for fieldfunc in propdatacontainer.fieldfuncs:
        field_array = sym.lambdify(T, fieldfunc.subs(calc.subsdict))(tgrid)

        # The field is only appended to the field file if the result of the
        # vectorized function call is an array. If the function always returns
        # zero, only a single value will be returned.
        if not isinstance(field_array, (float, int)):
            fieldslist.append(field_array)

    fields = np.stack(fieldslist, axis=0)
    hamiltonian_data = propdatacontainer.hamiltonian_data

    calc.wf = wf
    calc.efield = fields
    calc.umat = np.array(hamiltonian_data.umat, dtype=np.float64)
    calc.umat_inv = np.array(hamiltonian_data.umat ** -1, dtype=np.float64)
    calc.pot_diab = np.array(hamiltonian_data.ham_diab, dtype=np.float64)
    calc.pot_adiab = np.array(hamiltonian_data.ham_adiab, dtype=np.float64)

    final_path = calc.to_h5(remove_arrays=not keep_arrays)

    if return_calc:
        return final_path, calc

    return final_path
