"""
Custom types for the eQD package.

S.D.G.
"""


from typing import Any, NewType


SympyExpression = NewType("SympyExpression", Any)

PropagatorFunctionT = NewType("PropagatorFunctionT", Any)
