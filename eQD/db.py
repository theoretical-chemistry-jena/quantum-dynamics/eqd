"""
Module for generating and modifying databases for the calculation
objects.

S.D.G.

"""

import logging
import sqlite3
from pathlib import Path
from sqlite3 import Connection
from typing import Iterable, Union

from .calculation import Calculation, DBEntry


logger = logging.getLogger(__name__)


def get_db(
    location: Union[Path, str] = Path("."),
    dbname: str = "database.db",
    clean: bool = False,
) -> Connection:
    """
    Get or make a new database and directly add a well-behaved table called 'data'.

    Args:
        location (Union[Path, str], optional): Location of the database.
            Can be :memory: to specify an in-memory database.
            Defaults to Path(".").
        dbname (str, optional): Name of the database file Not used if the
            database is in-memory. Defaults to "database.db".
        clean (bool, optional): In case the database alreay exists, purge all the
            rows from the data table. Defaults to False.

    Returns:
        Connection: Database connection instance, from the sqlite3 module.
    """
    dbfile = (
        location
        if (location == ":memory:")
        else (Path(location).expanduser().resolve() / dbname)
    )

    conn = sqlite3.connect(dbfile)
    cursor = conn.cursor()

    gen_query = DBEntry.get_db_init_query()
    try:
        cursor.execute(gen_query)
    except sqlite3.OperationalError:
        logging.info("Database already exists.")

        if clean:
            logger.warning("Cleaning specified. Deleting all rows and starting fresh")
            response = cursor.execute("DELETE FROM data")
            logger.warning("Deleted {} rows.".format(response.rowcount))

    return conn


def add_calcs_to_db(
    connection: Connection, calculations: Iterable[Calculation], commit: bool = True
):
    """
    Take an iterator of Calculation instances and add them all to the database.
    Commits changes afterwards.

    Args:
        connection (sqlite3.Connection): Database connection object.

        calculations (Iterable[Calculation]): Calculations to add to the database.

        commit (bool): Whether the changes are directly commited to the database.
            Defaults to True.

    """

    entries = [x.dbentry for x in calculations]

    # Make paths relative to the database
    for _, name, filename in connection.execute('PRAGMA database_list'):
        if name == 'main' and filename is not None:
            dbpath = Path(filename).resolve().parent
            break

    for entry in entries:
        logger.debug(entry.__dict__)

        entry.path = str(Path(entry.path).relative_to(dbpath))
        logger.info("Relative path: {!s}".format(entry.path))

    query = entries[0].get_population_query()

    try:
        list_entries = [tuple(x) for x in entries]
        cursor = connection.executemany(query, list_entries)
    except sqlite3.IntegrityError as e:
        raise e

    logger.info("Added {} rows to the database.".format(cursor.rowcount))

    if commit:
        connection.commit()

    return None
