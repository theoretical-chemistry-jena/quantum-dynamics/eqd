"""
**eQD** is an **e**lectronic **Q**uantum **D**ynamics code based on the Split
operator method, developed by [Fabian G. Dröge](mailto:fabian.droege@uni-jena.de)
and others at the University of Jena.

## Usage

### Invocation from a command line
```bash
# As a Python module
python3 -m eQD input/sampleInput_2M.yaml

# As an executable
eQD input/sampleInput_2M.yaml
```

### Programmatic invokation

```python
>>> from eQD import main as eqd
>>> eqd(["path/to/inputfile", ...])
...
```

## Input files

Valid input files for different scenarios can be found at [the GitLab repo](
https://gitlab.com/theoretical-chemistry-jena/quantum-dynamics/eqd/-/tree/main/input
).

[S.D.G.](https://en.wikipedia.org/wiki/Soli_Deo_gloria)
"""

from .__main__ import main

from .calculation import Calculation

from .db import get_db, add_calcs_to_db

from .inputs import InputFactory

from . import integrators

from . import utils

from . import symbols

from . import _version
__version__ = _version.get_versions()['version']
