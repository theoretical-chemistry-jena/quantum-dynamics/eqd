"""
Global SymPy symbols and common electric field expressions used in the process.

S.D.G.
"""

import sympy as sym

# Define all the necessary sympy symbols.
(
    T,
    PHI,
    DT,
    W,
    EPSILON,
    V,
    J,
    DT1,
    DT2,
    DT3,
    MU,
    MU1,
    MU2,
    # Basic field
    E0,
    # NP field amplitude scaling beta(R)
    BETA,
    # Field enhancement, static
    ETA,
    TM,
    TP,
) = sym.symbols(
    "t phi dt omega epsilon V J dt_1 dt_2 dt_3 mu mu_1 mu_2 E0, beta, eta, tau_m, tau_p"
)

FIELD_CPHI = (E0 * sym.cos(W * T)) + (BETA * ETA * E0 * sym.cos(W * T + PHI + EPSILON))
