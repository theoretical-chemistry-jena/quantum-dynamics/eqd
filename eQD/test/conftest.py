"""
Shared fixtures for the pytest test suite of eQD.
"""


import shutil
import tempfile

from dataclasses import dataclass
from pathlib import Path
from typing import Union

import numpy as np
import pytest

from ..calculation import Calculation
from ..propagators import Propagator


@pytest.fixture(name="calc_full", params=[True, False])
def fixture_calc_full(request):
    """Fixture of a fully assembled Calculation instance"""
    ti = Calculation(
        n_monomers=2,
        tgrid=np.linspace(0, 10),
        wf=np.random.rand(3, 50),
        efield=np.random.rand(10),
        pot_diab=np.random.rand(3, 3),
        pot_adiab=np.random.rand(3, 3),
        umat=np.random.rand(3, 50),
        umat_inv=np.random.rand(10),
        omega=1.0,
        phi=0.5,
        epsilon=0.1,
        J=0.0,
        V=0.1,
        E0=1.0e-5,
        eta=1.0,
        beta=1.0,
        mus=(1.0, 1.0),
        taus=(1000.0, 1000.0),
        include_np_coupling=request.param,
        disable_damping=False,
        uphi=True,
        intcorr=False,
    )

    return ti


@pytest.fixture(name="n_states", params=[3, 4, 5])
def fixture_n_s(request):
    """Pytest fixture for number of states"""
    return request.param


@pytest.fixture(name="calc")
def fixture_sample_calc(n_states):
    """Sample calculation instance"""
    n_m = n_states - 1
    dic = {
        "n_monomers": n_m,
        "dt": 1.0,
        "tgrid": np.linspace(0, 10),
        "phi": 0.0,
        "epsilon": 0.0,
        "omega": 0.1,
        "J": 0.001,
        "V": 0.1,
        "mus": [1.0] * n_m,
        "taus": [1e4] * n_m,
        "E0": 1E-1,
        "beta": 1.0,
        "eta": 1.0,
        "disable_damping": False,
        "tau_coupled": False,
        "uphi": True,
        "intcorr": False,
    }

    return Calculation(**dic)


@pytest.fixture(name="umat3")
def fixture_sample_umat3():
    """Fixture for the analytic solution of the 3x3 unitary transform matrix"""

    umat = np.eye(3)
    umat[1:, 1:] = -0.5 * np.sqrt(2)
    umat[1, 1] *= -1

    return umat


@pytest.fixture(name="propgen")
def fixture_prop_generator(n_states):
    """
    Fixture for a propagator generator lambda function, based on a random complex matrix
    """

    randmat = (
        np.random.rand(n_states, n_states) + np.random.rand(n_states, n_states) * 1.0j
    )
    funcgen = lambda i: Propagator(
        split=2 ** i,
        lambdified=lambda t: randmat * t,
    )
    return funcgen


@pytest.fixture(name="propgen_eye")
def fixture_prop_generator_eye(n_states):
    """
    Fixture for a propagator generator lambda function, based on a complex identity matrix
    """

    propgen = lambda i: Propagator(
        split=2 ** i, lambdified=lambda t: np.eye(n_states, dtype=np.complex128) * t
    )

    return propgen


@dataclass
class FileAndFolder:
    """
    Small storage dataclass that holds a temporary file and the temporary directory
    used for a respective test.

    Attributes:
        tmpfile (Path): Temporary file to write to.
        tmpdir (Path): Temporary directory to write to.

    """

    tmpfile: Path
    tmpdir: Path


def create_tmp_input_and_folder(yamlfile: Union[Path, str]) -> FileAndFolder:
    """
    Creates a temporary input file with the output path substituted.

    Args:
        yamlfile (Union[Path, str]): Input file with the outpath specified as "@tmpdir@"

    Returns:
        FileAndFolder: Container namedtuple

    """
    srcfile = str(Path(__file__).parent / Path(yamlfile))
    destfile = tempfile.mkstemp(suffix="yml")[-1]
    tmpdir = tempfile.mkdtemp()

    with open(srcfile, "r", encoding="UTF-8") as rf:
        contents = rf.read().replace("@tmpdir@", tmpdir)

    with open(destfile, "w", encoding="UTF-8") as wf:
        wf.write(contents)

    return FileAndFolder(tmpfile=destfile, tmpdir=tmpdir)


def destroy_tmps(fileandfolder: FileAndFolder) -> None:
    """
    Destroys the temporary entities specified in the input

    Args:
        fileandfolder (FileAndFolder): Container namedtuple to hold information
            about the temporary files
    """

    destfile, tmpdir = fileandfolder.tmpfile, fileandfolder.tmpdir

    shutil.rmtree(tmpdir)
    Path(destfile).unlink()


@pytest.fixture(
    name="inputf",
    params=["testdata/testInput_single_2m.yaml", "testdata/testInput_steps_2m.yaml"],
)
def inputfile(request) -> str:

    fandf = create_tmp_input_and_folder(request.param)
    destfile = fandf.tmpfile

    # Yielding allows removing the temporary files later on.
    yield destfile

    destroy_tmps(fandf)
