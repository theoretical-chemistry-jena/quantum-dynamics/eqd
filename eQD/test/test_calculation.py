"""
Unit tests for the Calculation class.
"""

import logging
from pathlib import Path
import sqlite3

import pytest
import numpy as np

from ..calculation import Calculation
from ..db import add_calcs_to_db, get_db

logger = logging.getLogger(__name__)


def test_calculation_class_export(calc_full):
    """
    Assuring that Calculation.from_h5(t.to_h5()) == t
    """

    # pylint: disable=protected-access

    p = calc_full.to_h5()
    tc2 = Calculation.from_h5(p)

    logger.debug(repr(calc_full))
    logger.debug(repr(tc2))

    assert calc_full == tc2

    assert all(i is not None for i in tc2._get_public_attribs())
    assert all(i is not None for i in tc2._get_dataset_attribs())

    assert all(
        getattr(calc_full, key) == getattr(tc2, key)
        for key in tc2._get_public_attribs()
    )
    assert all(
        np.allclose(getattr(calc_full, key), getattr(tc2, key))
        for key in tc2._get_dataset_attribs()
    )

    logger.debug(tc2.__dict__)

    p.unlink()


@pytest.mark.parametrize(
    "fpath",
    [
        pytest.param((Path("."))),
        pytest.param(None, marks=pytest.mark.xfail),
    ],
)
def test_calculation_dbentry(calc_full: Calculation, fpath):
    """Test the database entry capabilities of the Calculation class"""

    calc_full._filepath = fpath  # pylint: disable=protected-access

    assert len(calc_full.dbentry) == 20


def test_calculation_c_db(tmpdir, calc_full: Calculation):
    """Test the database entry capabilities of the Calculation class"""

    tmpdir = Path(tmpdir)

    db = get_db(tmpdir)
    db.row_factory = sqlite3.Row

    calc_full.outpath = tmpdir
    calc_full.to_h5()

    add_calcs_to_db(db, [calc_full])

    entry = calc_full.dbentry
    assert len(entry) == 20

    for row_from_db in db.execute("SELECT * FROM data"):

        # We skip the first item as the path will be relativized in the process
        assert_list = [
            v1 == v2 for v1, v2 in zip(list(row_from_db)[1:], list(entry)[1:])
        ]

        logger.info(list(row_from_db)[1:])
        logger.info(list(entry)[1:])

        assert all(assert_list)
        assert row_from_db["path"] == Path(entry.path).name
