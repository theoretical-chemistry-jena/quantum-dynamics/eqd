import datetime
import logging

import numpy as np
import pytest
import scipy.optimize as sop
import sympy as sym
from eQD.symbols import T
from eQD.utils import (
    analytical_diag,
    envelope_exp2,
    gen_hamiltonian_sym,
    get_field_with_effect,
    get_formatted_today,
    phase_analyt,
)

logger = logging.getLogger(name=__name__)


def test_get_formatted_today():

    # Test only the date
    date = get_formatted_today()
    assert len(date) == 10
    assert date == datetime.date.today().isoformat()

    # Now with the time
    date_c_time = get_formatted_today(with_hours=True)
    assert len(date_c_time) == 16
    assert date_c_time[:10] == date


@pytest.fixture(params=[-0.00001, -0.001, -0.01, -0.05], name="j_coupling")
def fixture_jcoupling(request):
    """Small fixture for varying the J coupling"""
    return request.param


@pytest.fixture(params=[0.1, 0.5], name="potential_v")
def fixture_potentialv(request):
    """Small fixture for varying the excited state potential V"""
    return request.param


@pytest.fixture(params=[2, 4, 6], name="n_monomers")
def fixture_nmonomers(request):
    """Small fixture for even number of monomers"""
    return request.param


def test_gen_hamiltonian_sym(n_monomers, potential_v, j_coupling):
    """Test cases for the symmetric Hamiltonian"""
    V = potential_v
    J = j_coupling

    ham = np.asarray(gen_hamiltonian_sym(n_monomers + 1, V, J), float)

    assert np.all(np.logical_and(ham[0, :] == 0, ham[:, 0] == 0))
    assert np.all(ham.diagonal()[1:] == V)
    assert np.all(ham.diagonal(offset=1)[1:] == J)
    assert np.all(ham.diagonal(offset=-1)[1:] == J)


@pytest.fixture(params=["full", "ic", "pc"], name="effect_tpl")
def fixture_effects(request):
    """Fixture for the system"""
    return (
        request.param,
        {
            "full": {"uphi": True, "intcorr": False},
            "ic": {"uphi": True, "intcorr": True},
            "pc": {"uphi": False, "intcorr": False},
        }[request.param],
    )


def test_get_field_with_effect(calc, effect_tpl):
    """Testing the field generator"""
    eff_string, effect = effect_tpl

    for k, v in effect.items():
        setattr(calc, k, v)

    calc.epsilon = -0.01 * np.pi

    fields = get_field_with_effect(calc)

    assert len(fields) == calc.n_monomers
    assert len(fields[0].free_symbols) == 2

    if eff_string == "ic":
        phases = [
            phase_analyt(calc.E0, a, p)
            for a, p in zip(calc.np_amplitudes, calc.np_phases)
        ]
        field_ampls = [
            float(field.subs({"t": -phase, "omega": 1}))
            for field, phase in zip(fields, phases)
        ]
        assert np.allclose(field_ampls, field_ampls[0])

    if eff_string == "pc":
        field_ampls = [float(field.subs({"t": 0})) for field in fields]
        print(field_ampls)
        normalized_field_ampl = [
            float(field.subs({"t": 1.0, "omega": 0.123})) / ampl
            for ampl, field in zip(field_ampls, fields)
        ]
        assert np.allclose(normalized_field_ampl, normalized_field_ampl[0])

    if eff_string == "full":
        field_ampls = [float(field.subs({"t": 0})) for field in fields]
        normalized_field_ampl = [
            float(field.subs({"t": 1.0, "omega": 0.123})) / ampl
            for ampl, field in zip(field_ampls, fields)
        ]

        # In the full system, the phases and fields should *not* be identical!
        assert not np.allclose(field_ampls, field_ampls[0])
        assert not np.allclose(normalized_field_ampl, normalized_field_ampl[0])


def test_analytical_diag(n_monomers, potential_v, j_coupling):
    """Test cases for the analytical diagonalization"""

    if n_monomers not in [2, 4]:
        pytest.skip("Analytical diagonalization only implemented for 2M and 4M.")

    v = potential_v

    umat, ham_a = analytical_diag(n_monomers, V=v, J=j_coupling)
    ham_d = gen_hamiltonian_sym(n_monomers + 1, v, j_coupling)

    diagonal = np.diag(np.asarray(ham_a, float))
    ham_d_np = np.asarray(ham_d, float)
    ham_a_np = np.asarray(ham_a, float)
    umat_np = np.asarray(umat, float)
    # umat_inv_np = np.asarray(umat**-1, float)

    print("{!r}".format(ham_d[1:, 1:].eigenvects()))

    # ! A = PDP^-1
    reconstr_ham = umat_np @ ham_a_np @ umat_np.T

    # ! D = P^-1AP
    reconstr_diag = umat_np.T @ ham_d_np @ umat_np

    with np.printoptions(precision=3, linewidth=100, suppress=True):
        print("\n", repr(umat_np), repr(umat_np.T), sep="\n")
        print("\n", repr(ham_d_np), repr(reconstr_ham), sep="\n")
        print("\n", repr(ham_a_np), repr(reconstr_diag), sep="\n")

    assert np.all(diagonal.argsort() == np.arange(n_monomers + 1))

    assert np.allclose(np.asarray(umat ** -1 * umat, float), np.eye(n_monomers + 1))

    assert np.allclose(reconstr_ham, ham_d_np)
    assert np.allclose(ham_a_np, reconstr_diag)


@pytest.mark.parametrize(
    "t_central, sigma",
    [
        (50, 10),
        (50, 5),
        (100, 20),
    ],
)
def test_envelope_exp2(sigma, t_central):

    tgrid, dt = np.linspace(0, 200, num=1001, retstep=True)

    env = envelope_exp2(sigma, t_central)
    env_f = sym.lambdify([T], env)

    envelope = env_f(tgrid)

    # Calculate FWHM
    lower, upper = sop.fsolve(
        (lambda t: env_f(t) - 0.5), x0=[t_central - sigma / 2, t_central + sigma / 2]
    )
    fwhm = upper - lower

    # The maximum should be (close to) 1.0
    assert np.isclose(max(envelope), 1.0)

    # For this scenario, we approach the analytical integral
    assert np.isclose(
        np.sum(envelope) * dt, np.sqrt(2) * np.abs(sigma) * np.sqrt(np.pi)
    )

    # FWHM close to analytical FWHM
    assert np.isclose(fwhm, 2 * np.sqrt(2 * np.log(2)) * sigma)
