"""
S.D.G.

Pytest test suite for the eQD.propagators module
"""

import sqlite3

import numpy as np
import pytest

from ..__main__ import main
from ..calculation import Calculation
from .conftest import create_tmp_input_and_folder, destroy_tmps

#pylint: disable=protected-access

@pytest.mark.parametrize(
    "args, nr_files",
    [
        ([], 3),
        (["--disable-parallel"], 3),
        (["--disable-parallel", "--verbose"], 3),
        (["--disable-copy-input", "--disable-parallel"], 2),
        (["--disable-copy-input", "--disable-tqdm", "--disable-parallel"], 2),
    ],
)
def test_running_main(inputf, args, nr_files) -> None:

    ## Act
    factory = main([inputf, *args], return_factory=True)

    ## Assert
    # Number of files created
    opath = factory._outpath_full
    files = list(opath.glob("*"))
    assert len(files) == nr_files
    assert len(factory._calcs) == 1
    assert factory.disable_damping is False

    # Validity of the db
    db = list(opath.glob("*.db"))
    assert len(db) == 1

    rows = sqlite3.connect(db[0]).execute("SELECT * from data")
    assert (len(list(rows))) == 1


@pytest.mark.parametrize("file", [
    ("testdata/testInput_min_2m.yaml"),
    ("testdata/testInput_min_2m_implicit-dd.yaml"),
    ("testdata/testInput_min_2m_implicit-cNP.yaml"),
])
def test_minimal_input(file):
    """
    Functional test with minimal inputs
    """
    # Set up temporary data
    fandf = create_tmp_input_and_folder(file)

    # Act
    factory =  main([fandf.tmpfile], return_factory=True, keep_arrays=True)

    # Assert
    assert factory.disable_damping is True
    assert factory.include_np_coupling is False
    assert factory.taus is None
    assert factory.C_np is None
    assert factory.V_np is None

    assert len(factory._calcs) == 1

    for calc in factory._calcs:
        print(calc.wf)
        assert not np.any(np.isnan(calc.wf))

    # Do the cleanup
    destroy_tmps(fandf)


@pytest.mark.skip("Currently NP coupling is broken.")
def test_input_with_np():
    """
    Functional test involving simulation with included NP states
    """

    pytest.skip("Currently NP coupling is not implemented.")

    # Set up temporary data
    fandf = create_tmp_input_and_folder("testdata/testInput_full_2m_cNP.yaml")

    # Act
    factory =  main([fandf.tmpfile], return_factory=True, keep_arrays=True)

    # Assert
    assert factory.disable_damping is False
    assert factory.include_np_coupling is True
    assert factory.taus is not None
    assert factory.C_np is not None
    assert factory.V_np is not None

    assert len(factory._calcs) == 1

    for calc in factory._calcs:
        assert not np.any(np.isnan(calc.wf))

    for path in factory._paths:
        calc = Calculation.from_h5(path)
        assert not np.any(np.isnan(calc.wf))

    # Do the cleanup
    destroy_tmps(fandf)


@pytest.mark.parametrize("file", [
    ("testdata/testInput_sub_2m.yaml"),
])
def test_approximation_input(file):
    """
    Functional test including approximation of the subsystems
    """
    # Set up temporary data
    fandf = create_tmp_input_and_folder(file)

    # Act
    factory =  main([fandf.tmpfile, "--disable-parallel"], return_factory=True, keep_arrays=True)

    # Assert
    assert factory.disable_damping is True
    assert factory.taus is None
    assert len(factory._calcs) == 3

    for calc in factory._calcs:
        assert not np.any(np.isnan(calc.wf))

    # Do the cleanup
    destroy_tmps(fandf)
