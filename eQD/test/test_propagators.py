"""
S.D.G.

Pytest test suite for the eQD.propagators module
"""

import logging

import sympy as sym
import numpy as np
import pytest

from eQD.propagators import (
    Propagator,
    PropagatorAggregator,
    field_prop,
    fixed_jitted_lambdify,
    generate_props,
    recurse_prop,
    ti_damping_prop,
    ti_hamiltonian_prop,
)
from eQD.symbols import E0, T

logger = logging.getLogger(__name__)


@pytest.mark.parametrize(
    "split, func, arg, expected",
    [
        (1, lambda x: np.array(x + 1), 2, np.array(3)),
        (1, lambda x: np.array(x ** 2), 2, np.array(4)),
        (1, lambda x: np.array(x - 3), 2, np.array(-1)),
    ],
)
def test_propagator(split, func, arg, expected):
    """Test cases for the very small dataclass"""

    prop = Propagator(split, lambdified=func)

    # Assert identity
    assert prop == prop

    # Test whether hashing works
    logger.debug(hash(prop))

    assert np.all(prop(arg) == expected)


@pytest.mark.parametrize(
    "split, idx, field, t",
    [
        (2, 1, sym.sin(T), 1.0),
        (4, 2, (-1.0j * E0 * T), 1.0),
    ],
)
def test_field_prop(calc, split, idx, field, t: float):
    """Test case for the field propagator"""
    subs = {**calc.subsdict, "E_1": 5e-5}

    prop_sym = field_prop(calc, split, idx, field)

    func = sym.lambdify([T], prop_sym.subs(subs))

    # Assert the correct shape
    assert func(t).shape == (calc.n_states, calc.n_states)

    logger.debug(func(t))
    logger.debug(prop_sym)

    # Assert correct values
    assert np.allclose(func(t)[0, idx], func(t)[idx, 0])
    assert np.allclose(func(t)[0, 0], func(t)[idx, idx])

    logger.debug(np.real(func(t).diagonal()))

    assert all(
        v == 1.0
        for i, v in enumerate(np.real(func(t).diagonal()))
        if i not in [0, idx]
    )


def test_ti_hamiltonian_prop(calc):
    """Test case for time-independent Hamiltonian propagator"""

    prop, ham_data = ti_hamiltonian_prop(calc, split=1)

    # Test unitary transform matrices
    umat = np.asarray(ham_data.umat, dtype=np.float64)
    umat1 = np.asarray(np.linalg.inv(umat), dtype=np.float64)

    logger.debug(umat)
    logger.debug(umat1)
    logger.debug(umat @ umat1)

    assert np.allclose(umat1 @ umat, np.eye(calc.n_states))

    # Test diagonal Hamiltonian
    hamdiag = np.asarray(ham_data.ham_adiab, dtype=np.float64)
    hamdiab = np.asarray(ham_data.ham_diab, dtype=np.float64)

    # # * This might be a clever way to build the base Hamiltonian
    # jvals = [0] + [calc.J] * (calc.n_states - 2)
    # vvals = [0] + [calc.V] * (calc.n_states - 1)
    # hamdiab = np.diag(jvals, k=1)
    # hamdiab += hamdiab.T
    # hamdiab[np.diag_indices_from(hamdiab)] = vvals

    # Backtransform works.
    assert np.allclose(umat @ hamdiag @ umat1, hamdiab)

    # Test propagators
    prop = np.array(prop, dtype=np.complex128)

    logger.debug(prop)

    assert np.allclose(prop[1, 1], prop[2, 2])
    assert np.allclose(prop[1, 2], prop[2, 1])
    assert np.real(prop[0, 0]) == 1.0


def test_ti_damping_prop(calc, umat3):
    """Test case for time-independent damping propagator"""

    if not calc.n_states == 3:
        pytest.skip("Currently, there is no pre-prepared nD transform matrix.")

    # Using tau_coupled
    calc.tau_coupled = True
    prop_coupled = np.asarray(
        ti_damping_prop(calc, split=1, umat=sym.matrices.Matrix(umat3)),
        dtype=np.complex128,
    )

    calc.tau_coupled = False
    prop_uncoup = np.asarray(ti_damping_prop(calc, split=1), dtype=np.complex128)

    logger.debug(prop_coupled)
    logger.debug(prop_uncoup)

    assert np.allclose(np.linalg.inv(umat3) @ prop_coupled @ umat3, prop_uncoup)


def test_fixed_jitted_lambdify(n_states):
    """Test case for the test_fixed_jitted_lambdify function"""

    mat = sym.matrices.randMatrix(n_states) * 0.01j
    mat[0, 0] = 0.0

    logger.debug(mat)

    # Test whether it works
    testfct1 = fixed_jitted_lambdify(mat * T)
    assert np.all(testfct1(0.0) == 0.0)
    assert testfct1(0.0).dtype == np.complex128

    # Test whether we get the same results for a time-independent lambda func
    assert np.allclose(
        fixed_jitted_lambdify(mat)(0.0), np.asarray(mat, dtype=np.complex128)
    )


def test_generate_props(calc, n_states):
    """
    There is not much to test here, as most of the math
    is tested elsewhere. The Hamiltonian and umat things are
    checked above.
    """

    # With damping specified
    calc.disable_damping = False
    propcontainer = generate_props(calc)
    hamiltonian_data = propcontainer.hamiltonian_data

    # Assert correct number of props and fields
    assert len(propcontainer.props) == calc.n_monomers + 2
    assert len(propcontainer.fieldfuncs) == calc.n_monomers

    # Assert correct shaping
    assert all(
        p.lambdified(0.0).shape == (n_states, n_states) for p in [*propcontainer.props]
    )
    assert all(
        p.shape == (n_states, n_states)
        for p in [hamiltonian_data.ham_adiab, hamiltonian_data.umat]
    )

    # Assert correct order
    assert all(p.split == 2 ** i for i, p in enumerate(propcontainer.props))

    # Now disable damping
    calc.disable_damping = True
    propcontainer = generate_props(calc)
    assert len(propcontainer.props) == calc.n_monomers + 1


@pytest.mark.parametrize(
    "generator_spec, t, expected",
    [
        ("rand", 0.0, 0.0),
        ("eye", 1.0, "eye"),
    ],
)
def test_recurse_props(propgen, propgen_eye, n_states, generator_spec, t, expected):
    """Test the recursive propagator algorithm"""
    # Arrange
    if expected == "eye":
        expected_val = np.eye(n_states)
    else:
        expected_val = expected

    exps = list(range(5))
    generator = propgen if generator_spec == "rand" else propgen_eye
    funcs = [generator(i) for i in range(5)]

    # Act
    pfunc = recurse_prop(n_states=n_states, split_exps=exps, funcs=funcs)

    # Assert
    assert np.all(pfunc(t) == expected_val)


@pytest.mark.parametrize("nr_props", [(3), (4)])
def test_propagatoraggregator(propgen, n_states, nr_props):
    """Test for the PropagatorAggregator"""

    funcs = [propgen(i) for i in range(nr_props)]

    aggregator = PropagatorAggregator(funcs, n_states=n_states)

    logger.debug(aggregator(1.1))
    logger.debug(aggregator._naive_compute(1.1))  # pylint: disable=protected-access

    assert np.allclose(
        aggregator(1.0),
        aggregator._naive_compute(1.0),  # pylint: disable=protected-access
    )
