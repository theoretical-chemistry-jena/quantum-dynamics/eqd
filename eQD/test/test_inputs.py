"""
S.D.G.

"""

from pathlib import Path

import yaml
import pytest
import numpy as np


THIS = Path(__file__)


@pytest.mark.parametrize(
    "key,expected",
    [
        ("range", list(np.arange(10))),
        ("range-10", list(np.arange(0, 100, 10))),
    ],
)
def test_range(key, expected):

    fpath = THIS.parent / "testdata/yaml/test_range.yaml"

    with open(fpath, "r") as f:
        ymlc = yaml.load(f.read(), Loader=yaml.SafeLoader)

    rng = ymlc[key]

    assert rng.__next__() == 0
    assert len(rng) == 10
    assert list(rng) == expected


@pytest.mark.parametrize(
    "key,expected", [
        ("single_float", 1.0),
        ("single_bool", True),
        ("single_str", "Testing")
    ]
)
def test_single(key, expected):

    fpath = THIS.parent / "testdata/yaml/test_single.yaml"

    with open(fpath, "r") as f:
        ymlc = yaml.load(f.read(), Loader=yaml.SafeLoader)

    sng = ymlc[key]

    assert sng.__next__() == expected
    assert len(sng) == 1
    assert list(sng) == [expected]


@pytest.mark.parametrize(
    "key,expected", [
        ("steps_bool", [True, False]),
        ("steps_bool_yml", [True, False]),
        ("steps_float", [1.0, 2.0, 3.14]),
        ("steps_str", ["1.0", "2.0", "3.14"])
    ]
)
def test_steps(key, expected):

    fpath = THIS.parent / "testdata/yaml/test_steps.yaml"

    with open(fpath, "r") as f:
        ymlc = yaml.load(f.read(), Loader=yaml.SafeLoader)

    stp = ymlc[key]

    assert stp.__next__() == expected[0]
    assert len(stp) == len(expected)
    assert list(stp) == expected

