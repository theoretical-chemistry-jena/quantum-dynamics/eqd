"""
InputFactory and IterParameter classes for generating large-scale batch
computations for eQD.

The IterParameter class is a custom subclass to yaml.YAMLObject that allows population
directly from a YAML file. Usually you will never act on them directly.

The InputFactory provides a convenient entrypoint to the batch calculation
capabilities. InputFactory will generate the combinatoric itertools.product()
(cartesian product) of the specified input.

Usage:
```python
>>> file = "./input/sampleInput_4M.yaml"
>>> factory = InputFactory.from_file(file)
>>> factory.run_calcs()  # To run all calculations
...
```

or if you only want the calculation objects:

```python
>>> file = "./input/sampleInput_4M.yaml"
>>> calcs = InputFactory.from_file(file).construct_calcs()
```

S.D.G.
"""

from __future__ import annotations

import itertools as it
import logging
import shutil
from functools import partial
from multiprocessing import cpu_count
from pathlib import Path
from typing import Any, Iterable, List, Optional, Tuple, TypeVar, Union

import attr
import numpy as np
import yaml
from joblib.parallel import delayed
from tqdm.auto import tqdm

from .db import add_calcs_to_db, get_db
from .calculation import Calculation
from .integrators import integrator
from .utils import get_formatted_today, ProgressParallel

# Get the logger
logger = logging.getLogger(name=__name__)


@attr.s()
class IterParameter(yaml.YAMLObject):
    """
    Iterable parameter base class. Inherits yaml.YAMLObject to allow for
    direct population of the class from `yaml.load()`.
    """

    yaml_tag = "!_IterParam"
    yaml_loader = yaml.SafeLoader

    _array: Iterable = attr.ib(init=False, repr=False, factory=lambda: [])
    _stepsize: float = attr.ib(init=False, repr=False, default=0.0)
    _step: int = attr.ib(init=False, repr=False, default=0)
    _array_len: int = attr.ib(init=False, repr=False, default=-1)

    def __len__(self):
        if self._array_len == -1:
            self._array_len = len(self._array)

        return self._array_len

    def __iter__(self):
        self._step = 0
        return self

    def __next__(self):

        try:
            val = self._array[self._step]
            self._step += 1

            return val

        except IndexError:
            raise StopIteration  # pylint: disable=raise-missing-from

    @classmethod
    def from_yaml(cls, loader: yaml.Loader, node: yaml.MappingNode) -> IterParameter:
        """
        This is not to be used from a user side! Overriden classmethod from the original
        `.from_yaml()` method. Actually finishes initialization, which is crucial for our
        classes here.

        Args:
            loader (yaml.Loader): Loader to use.
            node (yaml.MappingNode): MappingNode to load the object from.

        Returns:
            IterParameter: The fully initialized iterable parameter.
        """
        fields = loader.construct_mapping(node, deep=True)
        return cls(**fields)


@attr.s()
class Single(IterParameter):
    """
    Iterable parameter for a single parameter value.
    Basically by definition a Singleton value.

    Attributes:
        item (Any): Data to be held by the single parameter.
    """

    yaml_tag = "!Single"
    item: Any = attr.ib()

    def __attrs_post_init__(self) -> None:
        self._array = [self.item]


@attr.s()
class Range(IterParameter):
    """
    Iterable parameter for a ranged parameter value. Ranges are inclusive and
    therefore include the rmin and rmax values. This class internally relies on
    `np.linspace()`, so caveats of this function apply.

    Attributes:
        rmin (Union[float, int]): Minimum value of the range.
        rmax (Union[float, int]): Maximum value of the range.
        n_steps (int): Number of steps.
        multiplier (Union[float, int]): Multiply the entire range by this factor.
            Defaults to 1.0.
    """

    yaml_tag = "!Range"

    rmin: Union[float, int] = attr.ib()
    rmax: Union[float, int] = attr.ib()
    n_steps: int = attr.ib()

    # Optional multiplier
    multiplier: float = attr.ib(default=1.0)

    def __attrs_post_init__(self) -> None:
        self._array, self._stepsize = np.linspace(
            self.rmin, self.rmax, num=self.n_steps, retstep=True
        )

        if self.multiplier != 1.0:
            self._array *= self.multiplier
            self._stepsize *= self.multiplier

    def linspace(
        self, retstep: bool = False
    ) -> Union[np.ndarray, Tuple[np.ndarray, float]]:
        """
        Return the underlying array as Numpy array. The notation and return value
        is kind of similar to the numpy.linspace function call.
        """
        if retstep:
            return self._array, self._stepsize
        else:
            return self._array


@attr.s()
class Steps(IterParameter):
    """
    Iterable parameter for a stepped parameter value.

    Attributes:
        steps (List[Any]): List of discrete parameter values to iterate through

    """

    yaml_tag = "!Steps"
    steps: List[Any] = attr.ib()

    def __attrs_post_init__(self) -> None:
        self._array = self.steps


ParamType = TypeVar("ParamType", Range, Steps, Single)


@attr.s(slots=True, repr=True)
class InputFactory:
    """
    Input factory that can generate calculations from a YAML input file.
    Important to note is that $\\hbar = 1$ for all situations.

    Attributes:
        cname (str): Calculation name, identifier of the calculation.

        n_monomers (int): Number of model monomers for the simulation.

        time (Range): float parameter. Time data, encoded in a RangeParameter instance.

        V (ParamType): float parameter. Diabatic excited state energy, in hartree
            units of energy.

        J (ParamType): float parameter. Diabatic inter-monomer excited state
            electronic coupling, in hartree

        phi (ParamType): float parameter. Static phase shift of the nanoparticle-emitted
            field, in radians.

        epsilon (ParamType): float parameter. Spatially dependent phase shift,
            in radians per monomer.

        omega (ParamType): float parameter. Excitation energy of the field, in hartree.

        E0 (ParamType): float parameter. Field strength of the incident field.
            Given in units of atomic units of field strength.
            # TODO: Reference?

        beta (ParamType): Spatial-coordinate dependent field amplitude scaling factor.
            For the monomer $n$ ($n \\in [0, ..., N-1]$), $\\beta_{eff} = \\beta^n$.

        eta (ParamType): NP-mediated field enhancement at $R=0$ (i.e., the position
            of the first monomer).

        mus (ParamType): List[float] parameter. Parameters contain a list of values
            for the diabatic transition dipole moment for each monomer. The length of a
            single entry has to be equal to the number of monomers.

        taus (ParamType): List[float] parameter.
            Parameters contain a list of values for the
            decoherence lifetime for each monomer. The length of a single entry has
            to be equal to the number of monomers. Given in units of atomic units of time.

        disable_damping (bool): Whether damping is disabled in this batch of calculations.
            Defaults to False.

        include_np_coupling (bool): Include nanoparticle coupling into the simulation.
            This is currently broken and specifying True will result in an error. # FIXME

        V_np (Optional[ParamType]): float parameter.
            Potential energy of the nanoparticle coupling state.
            Caveat see above.

        C_np (Optional[ParamType]): float parameter.
            Excited state coupling of the nanoparticle coupling state.
            Caveat see above.

        damp_excitonic (Union[Steps, Single]): bool parameter.
            Whether damping (decoherence) is applied in the adiabatic or
            diabatic basis. This does not affect anything as long as all values of `taus` are
            identical.

        approx_system (Union[Steps, Single]): Choice of `{"full", "intcorr", "phasecorr"}`.
            Whether approximations regarding fieldstrength and phase are to be applied.
            Currently only implemented for `n_monomers = 2`.

        outpath (Union[Path, str]): Path in which to save the calculation data
            and the database. Defaults to `Path(".")`.

        use_subfolder (bool): Whether to use a datetime-encoded subfolder for output.
            Defaults to True

        yaml_input (Union[Path, str]): The path to the YAML input file.
    """

    # Calculation metadata
    cname: str = attr.ib()
    n_monomers: int = attr.ib()

    # Simulation time
    time: Range = attr.ib()

    # Time-independent potential
    V: ParamType = attr.ib()
    J: ParamType = attr.ib()

    # Field specfication
    phi: ParamType = attr.ib()
    epsilon: ParamType = attr.ib()
    omega: ParamType = attr.ib()

    E0: ParamType = attr.ib()
    beta: ParamType = attr.ib(default=Single(item=1.0), kw_only=True)
    eta: ParamType = attr.ib(default=Single(item=1.0), kw_only=True)

    # Compound parameters
    mus: ParamType = attr.ib(default=None, kw_only=True)
    taus: ParamType = attr.ib(default=None, kw_only=True)

    disable_damping: bool = attr.ib(default=False)

    # NP coupling
    include_np_coupling: bool = attr.ib(default=False)
    V_np: Optional[ParamType] = attr.ib(default=None)
    C_np: Optional[ParamType] = attr.ib(default=None)

    # Pulse-related
    pulsed: bool = attr.ib(default=False)
    t_central: Optional[ParamType] = attr.ib(default=None)
    sigma: Optional[ParamType] = attr.ib(default=None)

    # Booleans
    damp_excitonic: Union[Steps, Single] = attr.ib(default=Single(item=True))

    # Applying approximations
    approx_system: Union[Steps, Single] = attr.ib(default=Single(item="Full"))

    # Output path
    outpath: Union[Path, str] = attr.ib(default=Path("."))
    use_subfolder: bool = attr.ib(default=True)

    # YAML input file
    _outpath_full: Union[Path, str] = attr.ib(default=None, init=False, kw_only=True)
    yaml_input: Union[Path, str] = attr.ib(default=None, kw_only=True)

    # Internal variables
    _calcs: List[Calculation] = attr.ib(init=False, factory=lambda: [], kw_only=True)
    _paths: List[Path] = attr.ib(init=False, factory=lambda: [], kw_only=True)

    def __attrs_post_init__(self):

        if self.use_subfolder:
            self._outpath_full = Path(self.outpath).expanduser().resolve() / "_".join(
                [get_formatted_today(with_hours=True), self.cname]
            )
        else:
            self._outpath_full = Path(self.outpath).expanduser().resolve()

        if self._outpath_full.is_dir():
            logger.warning(
                "Path '{p._outpath_full}' already exists. This might be problematic".format(
                    p=self
                )
            )
        else:
            self._outpath_full.mkdir(exist_ok=False, parents=True)
            logger.info(
                "Created new output directory '{p._outpath_full}'".format(p=self)
            )

        props = self._precheck_listbased_props()
        self._check_length_listbased_props(props)

        # FIXME: NP coupling
        if self.include_np_coupling:
            raise NotImplementedError("Currently, it's broken to have NP coupling included. Sorry")

    def _precheck_listbased_props(self) -> List[str]:

        props = ["mus"]

        # ! Checking damping disabled
        if self.disable_damping:
            self.taus = None
        # The implicit case
        elif getattr(self, "taus") is None and not self.disable_damping:
            logger.warning(
                "You did not explicitely disable/enable '{key}', but you did "
                "not specify '{assoc_key}'. I will disable it for you.".format(
                    key="disable_damping", assoc_key="taus"
                )
            )
            self.disable_damping = True
        else:
            # Check taus as well
            props += ["taus"]

        # ! Checking NP coupling
        if not self.include_np_coupling:
            # There is no implicit enabling!

            if any(i is not None for i in (self.V_np, self.C_np)):
                logger.warning(
                    "You did did not specify 'include_np_coupling = true' "
                    "in your input file but specified values for 'V_np' and 'C_np'. "
                    "This might be an oversight. Please look into that!"
                )

            self.V_np = None
            self.C_np = None

        else:
            props += ["C_np"]

        return props

    def _check_length_listbased_props(self, props):

        # * Abort, if any of the props is not set
        if any(getattr(self, prop) is None for prop in props):
            raise ValueError("You need to specify at least 'mus'.")

        # * Otherwise, check lengths of the parameters
        err_msg = "Length of the specified property '{prop}' ({l_prop}) != {n_m}. Aborting"

        for prop in props:
            value = getattr(self, prop)

            # * Single value
            if isinstance(value, Single):
                if not len(value.item) == self.n_monomers:
                    raise ValueError(
                        err_msg.format(
                            prop=prop, l_prop=len(value.item), n_m=self.n_monomers
                        )
                    )

            # * Stepped value
            elif isinstance(value, Steps):
                if not all(len(step) == self.n_monomers for step in value.steps):
                    raise ValueError(
                        err_msg.format(
                            prop=prop, l_prop=len(value.item), n_m=self.n_monomers
                        )
                    )
            # * Range value, not yet implemented
            else:
                raise NotImplementedError(
                    "Range parametrization not implemented for properties {!r}".format(
                        props
                    )
                )

    @classmethod
    def from_file(cls, yaml_file: Union[str, Path]) -> InputFactory:
        """
        Take a path to a yaml file and build an input from that.
        """

        filepath = Path(yaml_file).expanduser().resolve()

        with open(filepath, "r", encoding='UTF-8') as yf:
            dat = yaml.load(yf, Loader=yaml.SafeLoader)

        return cls(**dat, yaml_input=filepath)

    def construct_calcs(self) -> List[Calculation]:
        """Construct calculation instances based on the IterParameter-based input.

        Returns:
            List[Calculation]: List of all constructed Calculation instances.
                Also saved internally as self._calcs.
        """

        filter_list = [
            "cname",
            "n_monomers",
            "outpath",
            "time",
            "use_subfolder",
            "disable_damping",
            "yaml_input",
            "include_np_coupling",
            "pulsed"
        ]

        if self.disable_damping:
            filter_list.append("taus")

        if not self.include_np_coupling:
            filter_list.extend(["V_np", "C_np"])

        if not self.pulsed:
            filter_list.extend(["sigma", "t_central"])

        _filter = lambda x: x not in filter_list and x[0] != "_"
        public_vars = list(filter(_filter, self.__slots__))  # pylint: disable=no-member

        public_attribs = [getattr(self, x) for x in public_vars]
        tgrid, dt = self.time.linspace(retstep=True)

        calcs: List[Calculation] = []

        logger.debug(public_vars)
        logger.debug(public_attribs)

        for args in it.product(*public_attribs):

            rkwargs = dict(zip(public_vars, args))

            logger.debug(rkwargs)

            # Process rkwargs["damp_excitonic"]
            rkwargs["tau_coupled"] = rkwargs["damp_excitonic"]
            del rkwargs["damp_excitonic"]

            # Process rkwargs["approx_system"]
            rkwargs["uphi"], rkwargs["intcorr"] = {
                "full": (True, False),
                "intcorr": (True, True),
                "phasecorr": (False, False),
            }.get(rkwargs["approx_system"].lower())
            del rkwargs["approx_system"]

            kwargs = {
                "n_monomers": self.n_monomers,
                "cname": self.cname,
                "outpath": self._outpath_full,
                "tgrid": tgrid,
                "dt": dt,
                "disable_damping": self.disable_damping,
                "include_np_coupling": self.include_np_coupling,
                "pulsed": self.pulsed,
                **rkwargs,
            }

            calcs.append(Calculation(**kwargs))

        self._calcs = calcs
        logger.info("Constructed {} calculation instances.".format(len(self._calcs)))

        return self._calcs

    def run_calcs(
        self,
        disable_parallel: bool = False,
        disable_tqdm: bool = False,
        make_db: bool = True,
        copy_input: bool = True,
        keep_arrays: bool = False
    ) -> None:
        """
        Runner method that dispatches all the calculations that are prepared.
        If the calculations are not yet constructed, this will be done on-the-fly.

        Args:
            disable_parallel (bool): Whether to disable parallel execution
                of the calculations. Useful for debugging purposes. Defaults to False.

            disable_tqdm (bool): Whether to disable the progressbar. Useful
                for parallel execution, as streaming to tqdm does not seem to work
                properly from multiple processes. Defaults to False.

            make_db (bool): Whether to setup the calculation database.
                Defaults to True.

            copy_input (bool): Whether the input file is copied to the output directory.
                Defaults to True

            keep_arrays (bool): This controls whether calculation arrays are saved
                after outputting. This is sensible for very large calculations or
                those with many time steps. Defaults to True
        """

        if not self._calcs:
            self.construct_calcs()

        if copy_input and self.yaml_input is not None:
            shutil.copy(self.yaml_input, self._outpath_full / f"{self.cname}.input.yml")

        if disable_parallel:
            paths = []
            for calc in tqdm(self._calcs, disable=disable_tqdm):
                paths.append(integrator(calc, keep_arrays=keep_arrays))

            self._paths = paths

        else:
            curried_integrator = partial(integrator, return_calc=True, keep_arrays=keep_arrays)
            paths_calcs = ProgressParallel(
                total=len(self._calcs), n_jobs=cpu_count(), use_tqdm=not disable_tqdm
            )(delayed(curried_integrator)(calc) for calc in self._calcs)

            # As we use forking-type parallelism here, we need to gather the modified
            # calculations and reassign them.
            self._paths, self._calcs = list(zip(*paths_calcs))

        if make_db:
            # Get the correct location from the calculation-returned paths
            location = self._outpath_full

            # Make a database connection
            db_conn = get_db(
                location=location, dbname=".".join([self.cname, "db"]), clean=True
            )

            # Add all the calculations to the database
            add_calcs_to_db(db_conn, self._calcs)
