"""
Main CLI and module entrypoint for eQD -- Purely electronic Quantum dynamics

"""

import sys

import logging
from argparse import ArgumentParser, Namespace, Action, SUPPRESS
from pathlib import Path
from typing import List, Optional
from timeit import default_timer

from eQD.inputs import InputFactory
from eQD.utils import TqdmStream
from eQD._version import get_versions


class version_action(Action):
    """
    Custom argparse.Action to print the version string and exit.
    Adapted from https://stackoverflow.com/a/69789554/9940397.
    """

    def __init__(self, option_strings, dest, **kwargs):
        super().__init__(
            option_strings, dest, nargs=0, default=SUPPRESS, **kwargs
        )

    def __call__(self, parser, *args, **kwargs):
        print(get_versions()["version"])
        parser.exit()


def argparser(args: List[str]) -> Namespace:
    """
    CLI argument parser for eQD.

    Args:
        args (List[str]): Command line argument to be parsed.

    Returns:
        Namespace: Argument parser namespace with the arguments already parsed.
    """

    parser = ArgumentParser(
        description="Calculate Quantum dynamics using a purely electronic ansatz"
    )
    parser.add_argument("input", help="YAML file holding your inputs.")
    parser.add_argument("--verbose", "-v", action="count", default=0)
    parser.add_argument("--disable-parallel", "-s", action="store_true", default=False)
    parser.add_argument("--disable-tqdm", "-t", action="store_true", default=False)
    parser.add_argument(
        "--disable-copy-input", dest="copy_input", action="store_false", default=True
    )
    parser.add_argument(
        "--version", action=version_action, help="Show version of eQD and quit"
    )

    return parser.parse_args(args)


def show_welcome_msg():
    version = get_versions()["version"]
    print(
    f"""\n ✨ Welcome to eQD! You are using version {version}.
    """
    )


def main(
    argv: Optional[List[str]] = None,
    return_factory: bool = False,
    keep_arrays: bool = False,
) -> Optional[InputFactory]:
    """
    Main routine of eQD. Does some maintenance and setup and spawns the calculations.

    Args:
        argv (Optional[List[str]]): Command line arguments as a list of strings. Defaults
            to `sys.argv[1:]`.

        return_factory (bool): Flag to return the `InputFactory` that was created in
            the process. Mainly useful for testing and debugging.

    Raises:
        ValueError: If parsable arguments were not found.

    Returns:
        Optional[InputFactory]: The `InputFactory` object. Only returned if
            `return_factory == True`.
    """

    log_format = "[{levelname:^8}] @ {name}#{lineno}: {message}"
    logging.basicConfig(
        stream=TqdmStream, level=logging.WARNING, format=log_format, style="{"
    )

    if argv is None:
        raw_args = sys.argv[1:]
    else:
        raw_args = argv

    if raw_args == []:
        raise ValueError(
            "CLI args were not found either in function call or sys.argv. Aborting."
        )

    args = argparser(raw_args)

    disable_tqdm = args.disable_tqdm
    disable_parallel = args.disable_parallel
    copy_input = args.copy_input

    if args.verbose == 1:
        logging.getLogger().setLevel(logging.INFO)
    elif args.verbose > 1:
        logging.getLogger().setLevel(logging.DEBUG)

    show_welcome_msg()
    start_timer = default_timer()

    # Do the computations
    inputfile = Path(args.input).expanduser().resolve()
    factory = InputFactory.from_file(inputfile)
    factory.run_calcs(
        disable_parallel=disable_parallel,
        disable_tqdm=disable_tqdm,
        copy_input=copy_input,
        keep_arrays=keep_arrays,
    )

    print(
        "\n 🏇 Done. The calculations took {:.1f} s. Have a good day!".format(
            default_timer() - start_timer
        )
    )

    if return_factory:
        return factory


if __name__ == "__main__":
    main()
