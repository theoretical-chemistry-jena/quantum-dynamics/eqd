# -*- coding: utf-8 -*-
"""Setup file for pyQD, the split-operator TDSE package."""

from setuptools import setup
import versioneer

NAME = "eQD"
VERSION=versioneer.get_version()
CMDCLASS=versioneer.get_cmdclass()
DESCRIPTION = (
    "Python plotting utilities for QD simulation results"
)
URL = "https://gitlab.com/theoretical-chemistry-jena/quantum-dynamics/eQD"
EMAIL = "fabian.droege@uni-jena.de"
AUTHOR = "Fabian G. Dröge"
REQUIRES_PYTHON = ">=3.7.0"
REQUIRED = [
    "joblib",
    "h5py",
    "numba",
    "numpy",
    "opt_einsum",
    "pyyaml",
    "scipy",
    "sympy",
    "tqdm",
    "attrs"
]
ENTRYPOINTS = {
    "console_scripts": [
        "eQD = eQD.__main__:main"
    ]
}

CLASSIFIERS = [
    # Trove classifiers
    # Full list: https://pypi.python.org/pypi?%3Aaction=list_classifiers
    "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    "Programming Language :: Python",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3.7",
    "Programming Language :: Python :: 3.8",
    "Programming Language :: Python :: 3.9",
]

setup(
    name=NAME,
    description=DESCRIPTION,
    author=AUTHOR,
    author_email=EMAIL,
    url=URL,
    install_requires=REQUIRED,
    python_requires=REQUIRES_PYTHON,
    packages=[NAME],
    entry_points=ENTRYPOINTS,
    classifiers=CLASSIFIERS,
    zip_safe=False,
    license="GPLv3",
    include_package_data=False,
    version=VERSION,
    cmdclass=CMDCLASS,
)
