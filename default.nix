{ pkgs ? import ./nix/pkgs.nix
, doCheck ? true
}:

with pkgs; python3Packages.callPackage ./nix/eqd.nix { inherit doCheck; }