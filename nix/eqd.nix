{ requireFile, buildPythonPackage, lib, nix-gitignore
# Python dependencies
, sympy, numpy, numba, h5py, opt-einsum, tqdm, pytestCheckHook, pytest-cov
, matplotlib, jupyterlab, ipympl, pyyaml, joblib, scipy, attrs
, additionalDevDeps ? [ ]
, doCheck ? true
}:

buildPythonPackage rec {
    pname = "eqd";
    version = "0.3.0";

    nativeBuildInputs = additionalDevDeps ++ [ ];

    propagatedBuildInputs = [
        h5py
        joblib
        matplotlib
        numba
        numpy
        opt-einsum
        pyyaml
        scipy
        sympy
        tqdm
        attrs
    ];

    src = nix-gitignore.gitignoreSource ["notebooks"] ./..;

    checkInputs = [
      pytestCheckHook
      pytest-cov
    ];

    inherit doCheck;

    meta = with lib; {
      description = "Sympy-based Python implementation for electronic quantum dynamics simulation. using Split-Operator methodology.";
      license = licenses.gpl3Only;
      homepage = "https://git.rz.uni-jena.de/FabianGD/eQD";
      platforms = platforms.unix;
    };
}
