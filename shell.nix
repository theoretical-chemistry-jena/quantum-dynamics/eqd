{ pkgs ? import ./nix/pkgs.nix }:

let
  eqd = with pkgs; import ./default.nix { };

in
{
  production = with pkgs; with pkgs.python3Packages;
    mkShell {
      buildInputs = [

        # Standard
        which
        gnumake
        git

        # Custom Python packages
        eqd

        # Additional Python dependencies
        jupyterlab
        ipympl
      ];
    };

  dev = with pkgs; with pkgs.python3Packages;
    callPackage ./nix/eqd.nix {
      additionalDevDeps = [
        jupyterlab
        nbstripout
        ipympl

        memory_profiler
        versioneer
        black
        pylint
        pytest-cov
      ];
    };

  doc = with pkgs; with pkgs.python38Packages; callPackage ./nix/eqd.nix {
    additionalDevDeps = [
      pdoc3
    ];
  };
}

