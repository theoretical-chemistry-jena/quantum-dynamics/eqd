
.PHONY: docs

OPTS = --template-dir docs/template/ -o docs/

docs:
	pdoc3 eQD --html $(OPTS) --force

docserve:
	pdoc3 eQD --http : $(OPTS)


default: docs