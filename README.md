# eQD

[![pipeline status](https://gitlab.com/theoretical-chemistry-jena/quantum-dynamics/eqd/badges/main/pipeline.svg)](https://gitlab.com/theoretical-chemistry-jena/quantum-dynamics/eqd/-/commits/main)
[![coverage report](https://gitlab.com/theoretical-chemistry-jena/quantum-dynamics/eqd/badges/main/coverage.svg)](https://gitlab.com/theoretical-chemistry-jena/quantum-dynamics/eqd/-/commits/main)

[![built with nix](https://builtwithnix.org/badge.svg)](https://builtwithnix.org)

A purely electronic quantum dynamics simulation package, written in Python.

## Installation

The recommended way to install eQD and all it's dependencies is through the purely functional package manager [Nix](https://nixos.org/) (try it, it's actually pretty neat, you can download it [here](https://nixos.org/download.html#nix-quick-install)).

### As a system package

To install it in your system packages, clone the repo and run:

```bash
# Clone the reop
git clone https://gitlab.com/theoretical-chemistry-jena/quantum-dynamics/eqd
# Go into the directory
cd eqd
# Install eQD into your system packages
nix-env -f . -i
```

### Somewhere, potentially in an nix-shell environment

A even cleaner way is to use [niv](https://github.com/nmattia/niv), this allows you to use eQD from any folder you like in composition with other packages:

```bash
# Drop into a nix shell with niv
nix-shell -p niv

# Initialize niv somewhere
niv init -b nixpkgs-unstable

# Now, add the eQD repo to niv (tagged, stable version)
niv add -n eQD \
    -v 0.3.0 \
    -t 'https://gitlab.com/theoretical-chemistry-jena/quantum-dynamics/eqd/-/archive/<version>/eqd-<version>.tar.gz'

# Alternatively, to follow the main branch
niv add git -n eQD \
    -b main \
    --repo 'https://gitlab.com/theoretical-chemistry-jena/quantum-dynamics/eqd'
```

Then, you can build yourself a `shell.nix` file that gives you an environment to work in:

```nix
let
  sources = import ./nix/sources.nix;
  pkgs = import sources.nixpkgs { };
  eqd = import sources.eQD { inherit pkgs; };
in
with pkgs; mkShell {
  buildInputs = [
    eqd
     # ... and other packages you like
  ];
}
```

### The plain old Python way

If you do not have nix on your system, you can also install the package via `pip` from the cloned repository:

```bash
pip install .
```

## Usage

To use eQD, you need a yaml input file.
Samples for these kinds of files can be found in the `./input` folder for different number of monomers and settings.
eQD is built as a mass-input generator in the sense that a single input file is
able to specify a large number of individual calculations. In the input file, there are three possibilities of specifying a single parameter. Those are

- `!Single`: a single value,
- `!Steps`: a number of predefined values or
- `!Range`: a range of values calculated from start and end values and the number of steps required, possibly augmented by a multiplier.

You specify them the following way:

```yaml
single_param: !Single
  item: "value"

steps_param: !Steps
  steps:
    - "value 1"
    - "value 2"

range_param: !Range
  rmin: 0.0       # The starting value
  rmin: 10.0      # The end value
  n_steps: 40     # The number of steps to use
  multiplier: 1.0 # Optionally, a constant factor to multiply to
```

## Development / Contributing

Contributions via code reviews, bug reports, feature requests and pull requests are very welcome! :smile: To develop eQD, you can spawn a development shell from the root directory of the eQD repo via:

```bash
nix-shell -A dev
```

Python code is to be formatted with `black` and linted using `pylint` which also comes with the development shell.
